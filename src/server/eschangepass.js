const elasticsearch = require('elasticsearch');

module.exports = function(querystr,callback){
  let querystring = querystr.name;

  const esClient_post = new elasticsearch.Client({
    host:'localhost:9200',
    log:'error'
});
  esClient_post.search({
    index:"users",
    type:"wbwj",
    body:{
      query:{
        bool:{
          must:[
            {
              match:{
              account:querystr.username,
            }
          },{
              match:{
              password:querystr.password
            }
          }]
        }
      }
  }
  }).then(function(re){
    if(re.hits.hits[0]){
      esClient_post.update({
        index:"users",
        type:"wbwj",
        id:re.hits.hits[0]._id,
        body:{
          doc:querystr.data
        }
      }).then(function(res){
        callback(res);
      },function(err){
      console.trace(err.message);
    });
    }else{
      callback("400");
    }

  },function(err){
    console.trace(err.message);
  });

}
