export function ApproveTaskFinish(state = {finish_tasks:null}, action){
  switch (action.type) {
    case 'FinishData':
      return({
          finish_tasks:action.finish_tasks
        })
    default:
      return state
  }
}

export function ApproveFinish(finish_tasks) {
  return {type:'FinishData',finish_tasks:finish_tasks}
}
