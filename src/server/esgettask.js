const elasticsearch = require('elasticsearch');

module.exports = function(querystr,callback){
  const esClient_post = new elasticsearch.Client({
  host:'localhost:9200',
  log:'error'
});

    let taskquery = {
          query: {
            bool: {
              must: [
                {
                  match_phrase:{
                    key:querystr.key
                  }}
              ]
            }
          }
        }
    esClient_post.search({
      index:'tasks',
      type:'wbwj',
      body:taskquery,
    }).then(function(re){
      callback(re.hits.hits);
    },function(err){
      console.trace(err.message);
    });
}
