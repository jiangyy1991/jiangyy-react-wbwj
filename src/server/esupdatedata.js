const elasticsearch = require('elasticsearch');

module.exports = function(querystr,callback){
  let querystring = querystr.dataname.split('_');
  if(querystring.length!=1){
    querystr.dataname=querystring[1];
  }
  let str=[];
   for(var i in querystr.fields){
     str.push('ctx._source.'+i+' = params.'+i);
   };

  const esClient_post = new elasticsearch.Client({
    host:'localhost:9200',
    log:'error'
});
  esClient_post.updateByQuery({
    index:querystr.dataname,
    type:"wbwj",
    body:{
    query:{
      term:querystr.querystr
    },
    script:{
      inline:str.join(';'),
      params:querystr.fields
    }
  }
  }).then(function(re){
    callback(re);
  },function(err){
    console.trace(err.message);
  });

}
