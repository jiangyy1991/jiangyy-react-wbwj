import React from 'react';
import axios from 'axios';
import {Layout} from 'antd';
import {connect} from 'react-redux';
import ApproveSider from './sider';
import MainSider from './../../components/sider/mainsider';
import MainPage from './../../components/content/mainpage';
import CommonTables from './../../components/tables';
import Approvemodal from './../../components/modal/approvemodal';
import Viewmodal from './../../components/modal/viewmodal';
import {Link} from 'react-router-dom';


class AimList extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        username:null,
        unfinish_data: [],
        unfinish_columns: [],
        finish_data: [],
        finish_columns: []
      }
      this.refreshData = this.refreshData.bind(this);
    }
    handleClick = (e) => {
      console.log('click ', e);
    }
    componentWillMount() {
      var userinfo = sessionStorage.getItem('userinfo');
        if (this.props.UserInfo.account===null) {
          this.setState({
            username: JSON.parse(userinfo).account
          });
        }else{
        this.setState({
          username: this.props.account
        });
      }
      }
      componentDidMount(){
      let self = this;
      if(this.props.ApproveAimsUnfinish.unfinish_aims!==null){
        this.setState({
          unfinish_data:this.props.ApproveAimsUnfinish.unfinish_aims.map((item)=>
         item._source)
        })
      }else{
      axios({
        method: "GET",
        url: 'http://localhost:8901/app/data?data=unfinish_aims&&type=approve&username='+this.state.username,
        data: []
      }).then(function(result) {
        let aimsdata = result.data.map((item) =>
          item._source);
        self.setState({
          unfinish_data: aimsdata
        });
      }).catch(function(err) {
        console.log(err);
      });
    }
      axios({
        method: "GET",
        url: 'http://localhost:8901/app/fields?id=unfinish_aims_approve',
        data: []
      }).then(function(result) {
        self.setState({
          unfinish_columns: result.data.columns
        });
      }).catch(function(err) {
        console.log(err);
      });
      if(this.props.ApproveAimsFinish.finish_aims!==null){
        this.setState({
          finish_data:this.props.ApproveAimsFinish.finish_aims.map((item)=>
         item._source)
        })
      }else{
      axios({
        method: "GET",
        url: 'http://localhost:8901/app/data?type=approve&&data=finish_aims&username='+this.state.username,
        data: []
      }).then(function(result) {
        let aimsdata = result.data.map((item) =>
          item._source);
        self.setState({
          finish_data: aimsdata
        });
      }).catch(function(err) {
        console.log(err);
      });
    }
      axios({
        method: "GET",
        url: 'http://localhost:8901/app/fields?id=finish_aims_approve',
        data: []
      }).then(function(result) {
        self.setState({
          finish_columns: result.data.columns
        });
      }).catch(function(err) {
        console.log(err);
      });
  }

    refreshData() {
      let self = this;
      let username = this.state.username;
      axios({
        method: "GET",
        url: 'http://localhost:8901/app/data?data=unfinish_aims&&type=approve&username='+username,
        data: []
      }).then(function(result) {
        let aimsdata = result.data.map((item) =>
          item._source);
        self.setState({
          unfinish_data: aimsdata
        });
      }).catch(function(err) {
        console.log(err);
      });
      axios({
        method: "GET",
        url: 'http://localhost:8901/app/data?data=finish_aims&&type=approve&username='+username,
        data: []
      }).then(function(result) {
        let aimsdata = result.data.map((item) =>
          item._source);
        self.setState({
          finish_data: aimsdata
        });
      }).catch(function(err) {
        console.log(err);
      });

    }
    render() {
    function Displaytime(columns){
         if(columns.key!=='reservation'){
             return (text,record)=>(
                  <span>
                  {new Date(text).toLocaleString()}
                  </span>
              );
           }else{
             return (text,record)=>(
                  <span>
                  {new Date(text[0]).toLocaleDateString()+'~'+new Date(text[1]).toLocaleDateString()}
                  </span>
              );
           }
         }
   function DispAimStatus(columns){
     let task_state = {"0":"启动","1":"归档"};
     let aim_approve_state = {"0":"待审批","1":"通过","2":"拒绝"};
     let aim_option = {"0":"核查","1":"中标","2":"资源申请"};
     let aim_view = {"0":"只显号码","1":"只显内容","2":"全部显示"};

     switch (columns.key) {
     case 'aim_state':
      return (text,record)=>( <span>{task_state[text]} </span>);
      // break;
     case 'aim_approve_state':
      return (text,record)=>(<span>{aim_approve_state[text]} </span>);
      // break;
    case 'aim_option':
     return (text,record)=>( <span>{aim_option[text]} </span>);
      // break;
    case 'aim_view':
     return (text,record)=>(<span>{aim_view[text]} </span>);
      // break;
     default:
      return (text,record)=>(<span>{text}</span>);
          }
        }
        let unfinish_columns = this.state.unfinish_columns.slice();
        let username = this.state.username;
        for (var i in unfinish_columns) {
          if (unfinish_columns[i].type === "action") {
            unfinish_columns[i] = {
              title: unfinish_columns[i].cn,
              key: unfinish_columns[i].key,
              dataIndex: unfinish_columns[i].key,
              display: unfinish_columns[i].display,
              render: (text, record) => ( <div>
                  <Approvemodal
                  key = "approve"
                  name = "审批"
                  username = {username}
                  display = {unfinish_columns}
                  record = {record}
                  refreshData = {this.refreshData}
                  dataname = "unfinish_aims_approve" / >
                  </div>)
            };
          }else if (unfinish_columns[i].type === "time") {
            unfinish_columns[i] = {
              title: unfinish_columns[i].cn,
              key: unfinish_columns[i].key,
              dataIndex: unfinish_columns[i].key,
              display: unfinish_columns[i].display,
              render: Displaytime(unfinish_columns[i])
            };
          }else if (unfinish_columns[i].type === "num") {
            unfinish_columns[i] = {
              title: unfinish_columns[i].cn,
              key: unfinish_columns[i].key,
              dataIndex: unfinish_columns[i].key,
              display: unfinish_columns[i].display,
              render: DispAimStatus(unfinish_columns[i])
            };
          }else{
            unfinish_columns[i] = {
              title: unfinish_columns[i].cn,
              key: unfinish_columns[i].key,
              dataIndex: unfinish_columns[i].key,
              display: unfinish_columns[i].display,
              width: unfinish_columns[i].width
            };
          }
        }
        let finish_columns = this.state.finish_columns.slice();
        for (var j in finish_columns) {
          if (finish_columns[j].type === "action") {
            finish_columns[j] = {
              title: finish_columns[j].cn,
              key: finish_columns[j].key,
              dataIndex: finish_columns[j].key,
              display: finish_columns[j].display,
              render: (text, record) => ( <div>
                  <Viewmodal
                  key = "view"
                  name = "查看"
                  username = {username}
                  display = {finish_columns}
                  record = {record}
                  refreshData = {this.refreshData}
                  dataname = "finish_aims" / >
                  </div>)
            };
          }else if(finish_columns[j].type === "time") {
            finish_columns[j] = {
              title: finish_columns[j].cn,
              key: finish_columns[j].key,
              dataIndex: finish_columns[j].key,
              display: finish_columns[j].display,
              render: Displaytime(finish_columns[j])
            };
          }else if(finish_columns[j].type === "num") {
            finish_columns[j] = {
              title: finish_columns[j].cn,
              key: finish_columns[j].key,
              dataIndex: finish_columns[j].key,
              display: finish_columns[j].display,
              render: DispAimStatus(finish_columns[j])
            };
          }else{
            finish_columns[j] = {
              title: finish_columns[j].cn,
              key: finish_columns[j].key,
              dataIndex: finish_columns[j].key,
              display: finish_columns[j].display,
              width: finish_columns[j].width
            };
          }
        }

return (
  <Layout>
    <MainSider >
    <ApproveSider selectkey={"aims"} />
    </MainSider>
    <MainPage crumb={[<Link to={'/layout/approve/statistic'}>审批管理</Link>,"目标审批"]}>
      <CommonTables
      title = "待审批目标"
      columns = {unfinish_columns}
      data = {this.state.unfinish_data}
      tabletitle = {() => < div > < /div>} />
      <br / >
      <CommonTables
      title = "已审批目标"
      username = {username}
      columns = {finish_columns}
      data = {this.state.finish_data}
      tabletitle = {() => < div > < /div>} />
      </MainPage>
    </Layout>
        );
      }
    }

    export default connect(
      state=>({UserInfo:state.UserInfo,ApproveAimsFinish:state.ApproveAimsFinish,ApproveAimsUnfinish:state.ApproveAimsUnfinish})
    )(AimList);
