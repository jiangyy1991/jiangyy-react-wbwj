import React from 'react';
import TaskSider from './sider';
import { Layout,Card,Row,Col } from 'antd';
import {connect} from 'react-redux';
import axios from 'axios';
import {Link} from 'react-router-dom';
import MainSider from './../../components/sider/mainsider';
import MainPage from './../../components/content/mainpage';
import {Unfinish} from './../../reducer/task_unfinish';
import {Finish} from './../../reducer/task_finish';
import {AimFinish} from './../../reducer/aim_finish';
import {AimUnfinish} from './../../reducer/aim_unfinish';
import {Archived} from './../../reducer/task_archived';

class TaskStatistic extends React.Component{
  constructor(props){
    super(props);
    this.state={
      username:null,
      unfinish_tasks_count:0,
      finish_tasks_count:0,
      unfinish_aims_count:0,
      finish_aims_count:0,
      archived_tasks_count:0,
      finish_aims:0,
      recentview:[]
    }
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillMount() {
    var userinfo = sessionStorage.getItem('userinfo');
      if (this.props.account===null) {
        this.setState({
          username: JSON.parse(userinfo).account
        });
      }else{
      this.setState({
        username: this.props.account
      });
    }
    }
    componentDidMount(){
    let self = this;
    axios({
      method:"GET",
      url:'http://localhost:8901/app/data?type=users&data=finish_tasks&&username='+this.state.username,
      data:[]
        }).then(function(result) {
          self.setState({finish_tasks_count:result.data.length});
          self.props.dispatch(Finish(result.data));
       }).catch(function(err){
         console.log(err);
       });
    axios({
      method:"GET",
      url:'http://localhost:8901/app/data?type=users&data=unfinish_tasks&&username='+this.state.username,
      data:[]
        }).then(function(result) {
          self.setState({unfinish_tasks_count:result.data.length});
          self.props.dispatch(Unfinish(result.data));
       }).catch(function(err){
         console.log(err);
       });
    axios({
      method:"GET",
      url:'http://localhost:8901/app/data?type=users&data=archived_tasks&&username='+this.state.username,
      data:[]
        }).then(function(result) {
          self.setState({archived_tasks_count:result.data.length});
          self.props.dispatch(Archived(result.data));
       }).catch(function(err){
         console.log(err);
       });
    axios({
      method:"GET",
      url:'http://localhost:8901/app/data?type=users&data=unfinish_aims&&username='+this.state.username,
      data:[]
        }).then(function(result) {
          self.setState({unfinish_aims_count:result.data.length});
          self.props.dispatch(AimUnfinish(result.data));
       }).catch(function(err){
         console.log(err);
       });
    axios({
      method:"GET",
      url:'http://localhost:8901/app/data?type=users&data=finish_aims&&username='+this.state.username,
      data:[]
        }).then(function(result) {
          self.setState({finish_aims_count:result.data.length});
          self.props.dispatch(AimFinish(result.data));
       }).catch(function(err){
         console.log(err);
       });
   axios({
     method:"GET",
     url:'http://localhost:8901/app/recentview?username='+this.state.username,
     data:[]
       }).then(function(result) {
         let recentview = result.data.map(item=>
       item._source)
         self.setState({
           recentview:recentview});
      }).catch(function(err){
        console.log(err);
      });
  }
  handleChange(item){
    axios({
      method:"POST",
      url:'http://localhost:8901/app/recentview',
      datatype:'json',
      data:{dataname:'recentview',data:{"user":item.user,"action":'查看','task':item.task,'tabname':item.tabname,'aim':item.aim,'timeinterval':item.timeinterval,link:item.link,state:0,time:new Date().getTime()}}
    }).then(function(result) {
    //在这里将数据写入缓存方便最近查看页面从session中获取
    sessionStorage.setItem('recentview', JSON.stringify(item));
    }).catch(function(err){
    console.log(err);
  });
    axios({
      method:"POST",
      url:'http://localhost:8901/app/logstash',
      datatype:'json',
      data:{data:{user:item.users,action:'查看',time:new Date().getTime(),desp:'查看任务'+item.task+'下目标'+item.tabname.split('-')[0]+'的看板'+item.tabname.split('-')[1]+',查询条件：'+item.aim}}
        }).then(function(result) {
      console.log("插入日志");
    }).catch(function(err){
         console.log(err);
       });
  }
  render(){
    return(
      <Layout>
      <MainSider>
      <TaskSider selectkey={"statistic"} openkey={["task"]}/>
      </MainSider>
      <MainPage  crumb={["案件管理"]}>
      <div>
      <span style={{fontSize:'1.5em',color:'rgb(0, 0, 0, 0.85)',fontWeight:500}}><b>案件统计</b></span>
      <span style={{position:'absolute',margin: '0.8em 1em 1em 1.5em'}}>
      <Link to={'/layout/task/taskmanage'}>案件管理</Link>
      </span>
      <span style={{position:'absolute',margin: '0.8em 1em 1em 7em'}}>
      <Link to={'/layout/task/aimmanage'}>目标管理</Link>
      </span>
      </div>
      <br/>
      <Row gutter={16}>
      <Col span={6}>
      <Card title="案件">
        <p><Link to={'/layout/task/taskunfinish'}>待审批案件:{this.state.unfinish_tasks_count}</Link></p>
        <p><Link to={'/layout/task/taskfinish'}>已审批案件:{this.state.finish_tasks_count}</Link></p>
      </Card>
      </Col>
      <Col span={6}>
      <Card title="目标">
        <p><Link to={'/layout/task/aimunfinish'}>待审批目标:{this.state.unfinish_aims_count}</Link></p>
        <p><Link to={'/layout/task/aimfinish'}>已审批目标:{this.state.finish_aims_count}</Link></p>
      </Card>
      </Col>
      <Col span={6}>
      <Card title="已归档案件">
        <p><Link to={'/layout/task/archivedlist'}>已归档案件:{this.state.archived_tasks_count}</Link></p>
        <p><br /></p>
      </Card>
      </Col>
      </Row>
      <br />
      <div><h1>最近查看</h1></div>
      <Row gutter={16}>
      {this.state.recentview.map(item =>
        (<Col key={item.time} span={6}>
         <div style={{paddingTop:'4px'}}>
         <Card title={item.tabname}>
         <p>所属任务:{item.task}</p>
         <p ><Link to={'/layout/task/recentView'} onClick={()=>this.handleChange(item)}>目标内容:{item.aim}</Link></p>
         </Card>
         </div>
         </Col>))
       }
      </Row>
      </MainPage>
      </Layout>
    );
  }
}

export default connect(
  state=>state.UserInfo
)(TaskStatistic);
