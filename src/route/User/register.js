import React from 'react';
import {
  Link
} from 'react-router-dom';
import {
  Form,
  Input,
  Button,
  Divider
} from 'antd';
import 'antd/dist/antd.css';
import '../../css/assets/css/amazeui.min.css'
import '../../css/assets/css/amazeui.datatables.min.css'
import '../../css/assets/css/app.css'
import './register.css'
import axios from 'axios'
import Swal from 'sweetalert';
import PropTypes from "prop-types";
const FormItem = Form.Item;


class Register extends React.Component {
  static contextTypes = {
    router: PropTypes.object
  }
  constructor(props, context) {
    super(props, context);
    this.state = {
      confirmDirty:false,
      namesdata:[]
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleConfirmBlur = this.handleConfirmBlur.bind(this);
    this.compareToFirstPassword = this.compareToFirstPassword.bind(this);
    this.validateName = this.validateName.bind(this);
  }
  componentWillMount(){
      let self = this;
    axios({
      method:"GET",
      url:'http://localhost:8901/app/users?roles='+["0","1","2","3"],
      data:[]
        }).then(function(result) {
        let namesdata=result.data.map((item)=>
          item._source.account);
      self.setState({
       namesdata:namesdata
           });
       }).catch(function(err){
         console.log(err);
       });
  }
  handleConfirmBlur = (e) => {
      const value = e.target.value;
      this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    }
  compareToFirstPassword = (rule, value, callback) => {
      const form = this.props.form;
      if (value && value !== form.getFieldValue('password')) {
        callback('请确认您的密码是否输入相同!');
      } else {
        callback();
      }
    }
  validateToNextPassword = (rule, value, callback) => {
      const form = this.props.form;
      if (value && this.state.confirmDirty) {
        form.validateFields(['confirm'], { force: true });
      }
      callback();
    }
    validateName = (rule, value, callback) => {
    //    const form = this.props.form;
        if (this.state.namesdata.indexOf(value)!==-1 ) {
            callback('该用户名已被使用，请重新键入您的用户名');
        }else{
          callback();
        }
      }
  handleSubmit(event) {
    event.preventDefault();
    let history = this.context.router.history;
    var myDate = new Date();
    var uuid = "wbwj" + myDate.getFullYear() + myDate.getMonth() + myDate.getDate() + myDate.getHours() + myDate.getMinutes() + myDate.getSeconds() + myDate.getMilliseconds();

    this.props.form.validateFieldsAndScroll((err, values) => {
          if (!err) {
             axios({
               method: "POST",
               url: 'http://localhost:8901/app/register',
               data: {
                 account: values.account,
                 name: values.name,
                 roles: ["0"],
                 department: values.department,
                 password: values.password,
                 state: "0",
                 key: uuid
              }
            }).then(function(result) {
              if (result.status === 200) {
                Swal({
                  text: "注册成功！",
                  timer: 3000,
                  buttons: false
                }).then(function() {
                  history.push('/login');
                });
              } else {
                return false;
              }
            }).catch(function(err) {
              console.log(err);
            });
          }
        });
  }
  render() {
    const {
      getFieldDecorator,
      resetFields
    } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: {
          span: 24
        },
        sm: {
          span: 8
        },
      },
      wrapperCol: {
        xs: {
          span: 24
        },
        sm: {
          span: 16
        },
      },
    };
    const tailFormItemLayout = {
      labelCol: {
        xs: {
          span: 24
        },
        sm: {
          span: 8
        },
      },
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };
    return (
      <div className="am-g tpl-g">
      <div className="tpl-login">
      <div className = "tpl-login-content">
      <div>
      <div className = "title-style" > 用户注册 < /div>
      <Form onSubmit = {(event)=>this.handleSubmit(event)} >
      <FormItem
      { ...formItemLayout}
      label = "用户名" >
      {
        getFieldDecorator('account', {
          rules: [{
            required: true,
            message: '请输入用户名!',
          }, {
            validator: this.validateName,
          }],
        })(
          <Input placeholder='请输入用户名!'/ >
        )
      }
      </FormItem>
      <FormItem
      { ...formItemLayout}
      label = "真实姓名" >
      {
        getFieldDecorator('name', {
          rules: [{
            required: true,
            message: '请输入真实姓名!',
          }],
        })( <Input placeholder='请输入真实姓名!'/ >
        )
      }
      </FormItem>
      <FormItem
      { ...formItemLayout}
      label = "所属部门" >
      {
        getFieldDecorator('department', {
          rules: [{
            required: true,
            message: '请输入所在部门!',
          }],
        })(
          <Input  placeholder='请输入所在部门!'/ >
        )
      }
      </FormItem>

      <FormItem
      { ...formItemLayout}
      label = "密码" >
      {
        getFieldDecorator('password', {
          rules: [{
            required: true,
            message: '请输入您的密码!',
          }, {
            validator: this.validateToNextPassword,
          }],
        })( <Input type = "password"  placeholder='请输入密码!'/ >
        )
      }
      </FormItem>
      <FormItem
      { ...formItemLayout}
      label = "确认密码" >
      {
        getFieldDecorator('confirm', {
          rules: [{
            required: true,
            message: '请再次输入您的密码!',
          }, {
            validator: this.compareToFirstPassword,
          }],
        })( <Input type = "password"  placeholder='请再次输入密码!' onBlur = {this.handleConfirmBlur}/>
        )
      }
      </FormItem>
      <FormItem {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit">注册</Button>
          <Divider  key="divider" type="vertical" />
          <Button type="primary" onClick={()=>resetFields()}>重置</Button>

      </FormItem>
      <div className = "am-form-group" >
      <Link className = "linktologin"  to = "/login" > 返回到登录页面 < /Link>
      </div>
  </Form>
     </div>
     </div>
     </div>
     </div>
    );
  }
}

export default Form.create()(Register);
