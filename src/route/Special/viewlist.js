import React from 'react';
import axios from 'axios';
import {Table,Layout} from 'antd';
import MainPage from './../../components/content/mainpage';

class ViewList extends React.Component{
  constructor(props){
    super(props);
    this.state={
      data:[],
      columns:[],
      aims_data:[],
      aims_columns:[],
      username:null
    }
  }
  componentWillMount() {
    var userinfo = sessionStorage.getItem('userinfo');
    if (userinfo === null || userinfo === 'undefined') {
        this.props.history.push('/login');
    }else{
    let self=this;
    self.setState({
      username: JSON.parse(userinfo).account
    });
    axios({
      method:"GET",
      url:'http://localhost:8901/app/data?type=all&&data=tasks',
      data:[]
        }).then(function(result) {
          let tasksdata=result.data.map((item)=>
         item._source);
     self.setState({
      data:tasksdata
          });
       }).catch(function(err){
         console.log(err);
       });
   axios({
     method:"GET",
     url:'http://localhost:8901/app/fields?id=tasks',
     data:[]
       }).then(function(result) {
         self.setState({
           columns:result.data.columns
               });
      }).catch(function(err){
        console.log(err);
      });
  axios({
    method:"GET",
    url:'http://localhost:8901/app/fields?id=aims',
    data:[]
      }).then(function(result) {
        self.setState({
          aims_columns:result.data.columns
              });
     }).catch(function(err){
       console.log(err);
     });
   axios({
     method:"GET",
     url:'http://localhost:8901/app/data?type=all&&data=aims',
     data:[]
       }).then(function(result) {
         let aimsdata=result.data.map((item)=>
        item._source);
    self.setState({
     aims_data:aimsdata
         });
      }).catch(function(err){
        console.log(err);
      });
    }
  }

  render(){
  function Displaytime(columns){
       if(columns.key!=='reservation'){
           return (text,record)=>(
                <span>
                {new Date(text).toLocaleString()}
                </span>
            );
         }else{
           return (text,record)=>(
                <span>
                {new Date(text[0]).toLocaleDateString()+'~'+new Date(text[1]).toLocaleDateString()}
                </span>
            );
         }
       }
  function DispTaskStatus(columns){
   let task_state = {"0":"启动","1":"归档"};
   let task_approve_state = {"0":"待审批","1":"通过","2":"拒绝"};

      switch (columns.key) {
        case 'task_state':
         return (text,record)=>( <span>{task_state[text]} </span>);
      //    break;
        case 'task_approve_state':
         return (text,record)=>(<span>{task_approve_state[text]} </span>);
      //   break;
        default:
         return (text,record)=>(<span>{text}</span>);
      }
    }
  function DispAimStatus(columns){
    let task_state = {"0":"启动","1":"归档"};
    let aim_approve_state = {"0":"待审批","1":"通过","2":"拒绝"};
    let aim_option = {"0":"核查","1":"中标","2":"资源申请"};
    let aim_view = {"0":"只显号码","1":"只显内容","2":"全部显示"};

    switch (columns.key) {
    case 'aim_state':
     return (text,record)=>( <span>{task_state[text]} </span>);
    //  break;
    case 'aim_approve_state':
     return (text,record)=>(<span>{aim_approve_state[text]} </span>);
    //  break;
   case 'aim_option':
    return (text,record)=>( <span>{aim_option[text]} </span>);
    //  break;
   case 'aim_view':
    return (text,record)=>(<span>{aim_view[text]} </span>);
    //  break;
    default:
     return (text,record)=>(<span>{text}</span>);
         }
       }
  function DispTaskOther(columns){
     switch (columns.key) {
       case 'task_member':
        return (text,record)=>( <span>{text.join(' | ')} </span>);
      //  break;
       default:
        return (text,record)=>(<span>{text}</span>);
     }
   }
    let columns = this.state.columns.slice();
    //let username = this.state.username;
    for(var i in columns){
      if(columns[i].key!=='action'){
        if(columns[i].type==="time"){
          columns[i]={title:columns[i].cn,
            key:columns[i].key,
            dataIndex:columns[i].key,
            display:columns[i].display,
            render:Displaytime(columns[i])
          };
        }else if(columns[i].type==="num"){
          columns[i]={
            title:columns[i].cn,
            key:columns[i].key,
            dataIndex:columns[i].key,
            display:columns[i].display,
            render:DispTaskStatus(columns[i])
          };
        }else if(columns[i].type==="other"){
          columns[i]={
            title:columns[i].cn,
            key:columns[i].key,
            dataIndex:columns[i].key,
            display:columns[i].display,
            render:DispTaskOther(columns[i])
          };
        }else{
        columns[i]={
          title:columns[i].cn,
          key:columns[i].key,
          dataIndex:columns[i].key,
          display:columns[i].display,
          width:columns[i].width
        };
      }
      }
    }
    let aims_columns = this.state.aims_columns.slice();
    for(var j in aims_columns){
      if(aims_columns[j].key!=='action'){
        if(aims_columns[j].type==="time"){
          aims_columns[j]={
            title:aims_columns[j].cn,
            key:aims_columns[j].key,
            dataIndex:aims_columns[j].key,
            display:aims_columns[j].display,
            render:Displaytime(aims_columns[j])};
        }else if(aims_columns[j].type==="num"){
          aims_columns[j]={
            title:aims_columns[j].cn,
            key:aims_columns[j].key,
            dataIndex:aims_columns[j].key,
            display:aims_columns[j].display,
            render:DispAimStatus(aims_columns[j])};
        }else{
        aims_columns[j]={
          title:aims_columns[j].cn,
          key:aims_columns[j].key,
          dataIndex:aims_columns[j].key,
          display:aims_columns[j].display,
          width:aims_columns[j].width
          };
        }
      }
    }
    return(
      <Layout>
      <MainPage  crumb={["审批列表查看"]}>
      <h2><b>{"审批列表查看"}</b></h2>
      <br />
      <Table
      columns={columns}
      dataSource={this.state.data}
      expandedRowRender={(record)=>(
        <Table
        columns={aims_columns}
        dataSource={this.state.aims_data.filter(item=>item.aim_for===record.task_name)}
        />
      )}
      />
      </MainPage>
      </Layout>
    );
  }
}

export default ViewList;
