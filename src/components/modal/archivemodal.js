import React from 'react';
import {Form, Modal, Button} from 'antd';
import 'antd/dist/antd.css';
import axios from 'axios';
import Swal from 'sweetalert';

class  Archivemodal extends React.Component{
  constructor(props){
    super(props);
    this.state={
      visible:false
    }
    this.showModal = this.showModal.bind(this);
    this.handleOk = this.handleOk.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  showModal(){
    this.setState({
      visible:true,
    });
  }

  handleOk(){
  //  const { getFieldsValue, resetFields } = this.props.form;
    const { resetFields } = this.props.form;
    this.props.form.validateFieldsAndScroll((err, values) => {
    if (!err) {
    //const values = getFieldsValue();
    const refreshData = this.props.refreshData;
    var username = this.props.username;
    const fields = {task_state:"1"};
    const fieldname = ["task_state"];
    let aim_for = this.props.record.task_name;
    axios({
      method:"POST",
      url:'http://localhost:8901/app/update',
      datatype:'json',
      data:{querystr:{key:this.props.record.key},fieldname:fieldname,fields:fields,dataname:this.props.dataname}
        }).then(function(result) {
          axios({
            method:"POST",
            url:'http://localhost:8901/app/update',
            datatype:'json',
            data:{querystr:{aim_for:aim_for},fieldname:["aim_state"],fields:{aim_state:"1"},dataname:"aims"}
              }).then(function(result) {
                Swal("案件归档成功！").then(function(){
                    refreshData();
                    axios({
                      method:"POST",
                      url:'http://localhost:8901/app/logstash',
                      datatype:'json',
                      data:{data:{user:username,action:'归档',time:new Date().getTime(),desp:'归档案件'+aim_for+'及该案件下所以目标'}}
                        }).then(function(result) {
                      console.log("插入日志");
                    }).catch(function(err){
                         console.log(err);
                       });
                    }
                  )
             }).catch(function(err){
               console.log(err);
             });
       }).catch(function(err){
         console.log(err);
       });
    resetFields();
    this.setState({
      visible:false,
    });
  }
});
  }
  handleCancel(){
    this.setState({
      visible:false,
    });
  }

  render(){
    return(
      <span>
      <Button type="primary" onClick={this.showModal}>{this.props.name}</Button>
      <Modal
      title={this.props.name}
      visible={this.state.visible}
      onOk={this.handleOk}
      onCancel={this.handleCancel}
      okText="确认"
      cancelText="取消"
      >
      <div>
      你确定要归档该任务吗？归档后任务将不能重新启用！
      </div>
      </Modal>
      </span>
    );
  }
}

export default Form.create()(Archivemodal);
