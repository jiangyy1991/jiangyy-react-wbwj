const elasticsearch = require('elasticsearch');

module.exports = function(querystr,callback){
  const esClient_post = new elasticsearch.Client({
    host:'localhost:9200',
    log:'error'
});
  esClient_post.index({
    index:'users',
    type:'wbwj',
    body:querystr
  }).then(function(re){
    callback(re);
  },function(err){
    console.trace(err.message);
  });

}
