import React from 'react';
import { Route, Redirect } from 'react-router-dom';

class AuthManager extends React.Component {
  render() {
    const { component: Component, ...rest } = this.props
    const isManager = JSON.parse(sessionStorage.getItem("userinfo")).roles.indexOf("2") === -1 ? true : false;
    return (
        <Route {...rest} render={props => {
          return isManager
              ? <Redirect to="/layout/403" />
              : <Component {...props} />
        }} />
    )
  }
}

export default AuthManager
