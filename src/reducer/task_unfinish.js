export function TaskUnfinish(state = {unfinish_tasks:null}, action){
  switch (action.type) {
    case 'UnfinishData':
      return({
          unfinish_tasks:action.unfinish_tasks
        })
    default:
      return state
  }
}

export function Unfinish(unfinish_tasks) {
  return {type:'UnfinishData',unfinish_tasks:unfinish_tasks}
}
