import React from 'react';
import FormList from './../form/formlist';
import {Form, Modal, Button} from 'antd';
import 'antd/dist/antd.css';
import axios from 'axios';
import Swal from 'sweetalert';
import {withRouter} from 'react-router-dom';

class  Approvemodal extends React.Component{
  constructor(props){
    super(props);
    this.state={
      visible:false
    }
    this.showModal = this.showModal.bind(this);
    this.handleOk = this.handleOk.bind(this);
    this.handleNo = this.handleNo.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  showModal(){
    this.setState({
      visible:true,
    });
  }

  handleOk(){
    const { getFieldsValue, resetFields } = this.props.form;
    this.props.form.validateFieldsAndScroll((err, values) => {
    if (!err) {
    const values = getFieldsValue();
    const refreshData = this.props.refreshData;
    //let pathname = this.props.location.pathname;
    //const history = this.props.history;
    var username = this.props.username;
    const fields = (this.props.dataname==="unfinish_tasks_approve")?{task_approve_state:"1",task_approvetime:new Date().getTime()}:{aim_approve_state:"1",aim_approvetime:new Date().getTime()};
    const fieldname = (this.props.dataname==="unfinish_tasks_approve")?["task_approve_state","task_approvetime"]:["aim_approve_state","aim_approvetime"];
    let dataaims={unfinish_tasks_approve:'案件',unfinish_aims_approve:'目标'};
    let datatype={unfinish_tasks_approve:values.task_name,unfinish_aims_approve:values.aim_name};
    let dataname = this.props.dataname;
    axios({
      method:"POST",
      url:'http://localhost:8901/app/update',
      datatype:'json',
      data:{querystr:{key:this.props.record.key},fieldname:fieldname,fields:fields,dataname:this.props.dataname}
        }).then(function(result) {
          Swal("审批完成！").then(function(){
              refreshData();
              axios({
                method:"POST",
                url:'http://localhost:8901/app/logstash',
                datatype:'json',
                data:{data:{user:username,action:'审批通过',time:new Date().getTime(),desp:'审批通过'+dataaims[dataname]+datatype[dataname]}}
                  }).then(function(result) {
                console.log("插入日志");
              }).catch(function(err){
                   console.log(err);
                 });
              }
            )
       }).catch(function(err){
         console.log(err);
       });
    resetFields();
    this.setState({
      visible:false,
    });
  }
});
  }

  handleNo(){
    //调用同一接口，驳回操作与审批操作需要参数区别
    const { getFieldsValue, resetFields } = this.props.form;
    this.props.form.validateFieldsAndScroll((err, values) => {
    if (!err) {
    const values = getFieldsValue();
    const refreshData = this.props.refreshData;
    var username = this.props.username;
    let dataaims={unfinish_tasks_approve:'案件',unfinish_aims_approve:'目标'};
    let datatype={unfinish_tasks_approve:values.task_name,unfinish_aims_approve:values.aim_name};
    let dataname = this.props.dataname;

    const fields = (this.props.dataname==="unfinish_tasks_approve")?{task_approve_state:"2",task_approvetime:new Date().getTime()}:{aim_approve_state:"2",aim_approvetime:new Date().getTime()};
    axios({
      method:"POST",
      url:'http://localhost:8901/app/update',
      datatype:'json',
      data:{querystr:{key:this.props.record.key},fields:fields,dataname:this.props.dataname}
        }).then(function(result) {
          Swal("审批完成！").then(function(){
              refreshData();
              axios({
                method:"POST",
                url:'http://localhost:8901/app/logstash',
                datatype:'json',
                data:{data:{user:username,action:'审批拒绝',time:new Date().getTime(),desp:'审批拒绝'+dataaims[dataname]+datatype[dataname]}}
                  }).then(function(result) {
                console.log("插入日志");
              }).catch(function(err){
                   console.log(err);
                 });
              }
            )
       }).catch(function(err){
         console.log(err);
       });
    resetFields();
    this.setState({
      visible:false,
    });
  }
  });
  }

  handleCancel(){
    const {resetFields}= this.props.form;
    resetFields();
    this.setState({
      visible:false,
    });
  }
  render(){
    let displays = this.props.display.slice();
    let disabled=[];
    for(var i in displays){
      if(displays[i].display!=='none'){
        disabled[displays[i].key]=true;
      }
    }
    return(
      <span>
      <Button type="primary" onClick={this.showModal}>{this.props.name}</Button>
      <Modal
      title={this.props.name}
      visible={this.state.visible}
      onOk={this.handleOk}
      onCancel={this.handleCancel}
      footer={[
            <Button key="back" onClick={this.handleCancel}>取消</Button>,
            <Button key="no" type="primary" onClick={this.handleNo}>拒绝</Button>,
            <Button key="submit" type="primary" onClick={this.handleOk}>通过</Button>,
          ]}
      >
      <FormList form={this.props.form} display={this.props.display} record={this.props.record} disabled={disabled}/>
      </Modal>
      </span>
    );
  }
}

export default withRouter(Form.create()(Approvemodal));
