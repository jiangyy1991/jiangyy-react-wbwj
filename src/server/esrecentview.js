const elasticsearch = require('elasticsearch');

module.exports = function(querystr,callback){
  const esClient_post = new elasticsearch.Client({
  host:'localhost:9200',
  log:'error'
});

    let recentview = {
          query: {
            bool: {
              must: [
                {
                  match_phrase:{
                    user:querystr.username
                  }}
              ]
            }
          },
          size:12,
          sort: { time: { order: "desc" }}
        }
    esClient_post.search({
      index:'recentview',
      type:'wbwj',
      body:recentview,
    }).then(function(re){
      callback(re.hits.hits);
    },function(err){
      console.trace(err.message);
    });
}
