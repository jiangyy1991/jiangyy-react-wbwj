import React from 'react';

import {Table} from 'antd';

class CommonTables extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      filteredInfo: null,
      sortedInfo: null,
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(pagination,filters,sorter){
      this.setState({
        sortedInfo: sorter,
      });
    }

  render(){

    return(
      <div>
      <h2><b>{this.props.title}</b></h2>
      <Table
      bordered
      columns={this.props.columns}
      dataSource={this.props.data}
      onChange={this.handleChange}
      title={this.props.tabletitle}
      />
      </div>
    );
  }
}

export default CommonTables;
