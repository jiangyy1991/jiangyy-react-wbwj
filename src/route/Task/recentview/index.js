import React from 'react';
import TaskSider from './../sider';
import {Layout} from 'antd';
import MainSider from './../../../components/sider/mainsider';
import MainPage from './../../../components/content/mainpage';
import {Link} from 'react-router-dom';

class RecentView extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      iFrameHeight:'0px',
      link:null,
      keyword:null,
      timeinterval:[]
    }
  }
  componentWillMount(){
    var data = JSON.parse(sessionStorage.getItem('recentview'));
    this.setState({
      keyword:data.aim,
      timeinterval:data.timeinterval,
      link:data.link
    });
  }

  render() {
    return (
    <Layout>
    <MainSider>
    <TaskSider selectkey={"statistic"} openkey={["task"]}/>
    </MainSider>
    <MainPage  crumb={[<Link to={'/layout/task/statistic'}>案件管理</Link>,'最近查看']}>
    <
      iframe style = {
        {
          width: '100%',
          height: this.state.iFrameHeight,
          overflow: 'visible'
        }
      }
      onLoad = {
        () => {
          this.setState({
            "iFrameHeight": '800px'
          });
        }
      }
      ref = "iframe"
      title={this.state.timeinterval}
      src = {"http://localhost:5601/app/kibana#/home?_g=()"}
      width = "100%"
      height = {
        this.state.iFrameHeight
      }
      scrolling = "no"
      frameBorder = "0" /
      >
      </MainPage>
      </Layout>
    );
  }
}

export default RecentView;
