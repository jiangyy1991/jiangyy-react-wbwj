export function TaskFinish(state = {finish_tasks:null}, action){
  switch (action.type) {
    case 'FinishData':
      return({
          finish_tasks:action.finish_tasks
        })
    default:
      return state
  }
}

export function Finish(finish_tasks) {
  return {type:'FinishData',finish_tasks:finish_tasks}
}
