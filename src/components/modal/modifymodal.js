import React from 'react';
import FormList from './../form/formlist';
import {Form, Modal, Button} from 'antd';
import 'antd/dist/antd.css';
import axios from 'axios';
import Swal from 'sweetalert';

class  Modifymodal extends React.Component{
  constructor(props){
    super(props);
    this.state={
      visible:false
    }
    this.showModal = this.showModal.bind(this);
    this.handleOk = this.handleOk.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  showModal(){
    this.setState({
      visible:true,
    });
  }

  handleOk(){
    const { getFieldsValue, resetFields } = this.props.form;
    this.props.form.validateFieldsAndScroll((err, values) => {
    if (!err) {
    const values = getFieldsValue();
    const refreshData = this.props.refreshData;
    var username = this.props.username;
    axios({
      method:"POST",
      url:'http://localhost:8901/app/update',
      datatype:'json',
      data:{querystr:{key:this.props.record.key},fields:values,dataname:this.props.dataname}
        }).then(function(result) {
          Swal("修改成功").then(function(){
              refreshData();
              axios({
                method:"POST",
                url:'http://localhost:8901/app/logstash',
                datatype:'json',
                data:{data:{user:username,action:'修改',time:new Date().getTime(),desp:'修改已审批案件'+values.task_name+'参与人员为：审批人'+values.task_approver+',参与人'+values.task_member+',责任人'+values.task_resp}}
                  }).then(function(result) {
                console.log("插入日志");
              }).catch(function(err){
                   console.log(err);
                 });
              }
            )
       }).catch(function(err){
         console.log(err);
       });
    resetFields();
    this.setState({
      visible:false,
    });
  }
});
  }
  handleCancel(){
    this.setState({
      visible:false,
    });
  }

  render(){
    let displays = this.props.display.slice();
    let disabled=[];
    for(var i in displays){
      if(displays[i].display!=='none'){
        if(displays[i].dataIndex!=="task_resp"&&displays[i].dataIndex!=="task_member"&&displays[i].dataIndex!=="task_approver"){
          disabled[displays[i].key]=true;
        }else{
          disabled[displays[i].key]=false;
        }
      }
    }
    return(
      <span>
      <Button type="primary" onClick={this.showModal}>{this.props.name}</Button>
      <Modal
      title={this.props.name}
      visible={this.state.visible}
      onOk={this.handleOk}
      onCancel={this.handleCancel}
      okText="确认"
      cancelText="取消"
      >
      <FormList form={this.props.form} disabled={disabled} display={this.props.display} record={this.props.record}/>
      </Modal>
      </span>
    );
  }
}

export default Form.create()(Modifymodal);
