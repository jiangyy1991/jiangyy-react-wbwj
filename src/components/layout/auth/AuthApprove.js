import React from 'react';
import { Route, Redirect } from 'react-router-dom';

class AuthApprove extends React.Component {
  render() {
    const { component: Component, ...rest } = this.props;
    const isApprove = JSON.parse(sessionStorage.getItem("userinfo")).roles.indexOf("1") === -1 ? true : false;
    return (
        <Route {...rest} render={props => {
          return isApprove
              ? <Redirect to="/layout/403" />
              : <Component {...props} />
        }} />
    )
  }
}

export default AuthApprove
