export function TaskArchived(state = {archived_tasks:null}, action){
  switch (action.type) {
    case 'ArchivedData':
      return({
          archived_tasks:action.archived_tasks
        })
    default:
      return state
  }
}

export function Archived(archived_tasks) {
  return {type:'ArchivedData',archived_tasks:archived_tasks}
}
