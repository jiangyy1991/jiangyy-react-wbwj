const elasticsearch = require('elasticsearch');

module.exports = function(querystr,callback){

  const esClient_post = new elasticsearch.Client({
    host:'localhost:9200',
    log:'error'
});
let querybody={
query:{
  bool:{
    must:[
      {
        match_phrase:{
        user:querystr.data.user
      }},
    {
      match_phrase:{
      task:querystr.data.task
    }},
    {
      match_phrase:{
      tabname:querystr.data.tabname
    }},
    {
      match_phrase:{
      aim:querystr.data.aim
    }}]
}}};
esClient_post.search({
  index:"recentview",
  type:"wbwj",
  body:querybody
}).then(function(re){
  if(re.hits.hits.length===0){
    esClient_post.index({
      index:querystr.dataname,
      type:'wbwj',
      body:querystr.data
    }).then(function(re){
      callback(re);
    },function(err){
      console.trace(err.message);
    });
  }else{
    esClient_post.update({
      index:"recentview",
      type:"wbwj",
      id:re.hits.hits[0]._id,
      body:{doc:querystr.data}
    }).then(function(re){
      callback(re);
    },function(err){
      console.trace(err.message);
    });
  }
},function(err){
  console.trace(err.message);
});
}
