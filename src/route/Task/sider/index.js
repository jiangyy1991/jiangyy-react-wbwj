import React from 'react';
import {Link} from 'react-router-dom';
import {Menu,Badge} from 'antd';
import axios from 'axios';

const SubMenu = Menu.SubMenu;

class TaskSider extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      aims:[],
      aimscount:[],
      username:null
    }
  }
  componentWillMount() {
    var userinfo = sessionStorage.getItem('userinfo');
    if (userinfo === null || userinfo === 'undefined') {
        this.props.history.push('/login');
    }else{
    let self=this;
    self.setState({
      username: JSON.parse(userinfo).account
    });
    axios({
      method: "GET",
      url: 'http://localhost:8901/app/contentfors?account=' + JSON.parse(userinfo).account,
      data: []
    }).then(function(result) {
      let tasksdata = result.data.map((item) =>
        item._source.task_name);
      self.setState({
        aims: tasksdata
      });
    }).catch(function(err) {
      console.log(err);
    });
    axios({
      method: "GET",
      url: 'http://localhost:8901/app/aimscount',
      data: []
    }).then(function(result) {
      self.setState({
        aimscount: result.data
      })
    }).catch(function(err) {
      console.log(err);
    });
  }
}
  render(){
    let aims = this.state.aims.slice();
    let aimscount = this.state.aimscount.slice();
    let datacount=[];
    aimscount.map(item=>
    datacount[item.key]=item.doc_count);
    return(
      <Menu
        onClick={this.handleClick}
        defaultSelectedKeys={[this.props.selectkey]}
        defaultOpenKeys={this.props.openkey}
        mode="inline"
      >
      <SubMenu key="task" title={<span>案件查看</span>}>
        <Menu.Item key="statistic"><Link to={'/layout/task/statistic'}>案件统计</Link></Menu.Item>
        <SubMenu key="tasks" title="案件列表">
        {
          aims.map(item=>
          <SubMenu key={item} title={<span>{item}{" "}<Badge count={datacount[item]} style={{ backgroundColor: '#52c41a' , boxShadow: '0 0 0 1px #d9d9d9 inset' }} /></span>}>
          <Menu.Item key={item+'/核查'}>核查<Link to={'/layout/task/tasks/'+item+'/核查'} onClick={this.props.changePanes}/></Menu.Item>
          </SubMenu>)
        }
          </SubMenu>
        <Menu.Item key="archived"><Link to={'/layout/task/archivedlist'}>已归档案件</Link></Menu.Item>
      </SubMenu>
      </Menu>
    )
  }
}

export default TaskSider;
