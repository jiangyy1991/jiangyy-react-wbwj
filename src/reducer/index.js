//把引入的reducer整合成一个传出去
import { combineReducers } from 'redux';
import { AimsFinish } from './aim_finish';
import { AimsUnfinish } from './aim_unfinish';
import {TaskFinish} from './task_finish';
import {TaskUnfinish} from './task_unfinish';
import {UserInfo} from './userinfo';
import {TaskArchived} from './task_archived';
import {ApproveTaskFinish} from './approve_task_finish';
import {ApproveTaskUnfinish} from './approve_task_unfinish';
import { ApproveAimsFinish } from './approve_aim_finish';
import { ApproveAimsUnfinish } from './approve_aim_unfinish';

export default combineReducers({
  AimsFinish,
  AimsUnfinish,
  TaskFinish,
  TaskUnfinish,
  TaskArchived,
  UserInfo,
  ApproveAimsFinish,
  ApproveAimsUnfinish,
  ApproveTaskFinish,
  ApproveTaskUnfinish
})
