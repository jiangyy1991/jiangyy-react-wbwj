import React from 'react';
import axios from 'axios';
import {Divider,Layout} from 'antd';
import MainPage from './../../../components/content/mainpage';
import CommonTables from './../../../components/tables';
import Addmodal from './../../../components/modal/addmodal';
import Commonmodal from './../../../components/modal/commonmodal';
import Deletemodal from './../../../components/modal/deletemodal';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';

class AimUnfinish extends React.Component{
  constructor(props){
    super(props);
    this.state={
      unfinish_data:[],
      unfinish_columns:[],
      username:null
    }
    this.refreshData = this.refreshData.bind(this);
  }
  componentWillMount() {
    var userinfo = sessionStorage.getItem('userinfo');
      if (this.props.UserInfo.account===null) {
        this.setState({
          username: JSON.parse(userinfo).account
        });
      }else{
      this.setState({
        username: this.props.account
      });
    }
    }
    componentDidMount(){
    let self = this;
    if(this.props.AimsUnfinish.unfinish_aims!==null){
      this.setState({
        unfinish_data:this.props.AimsUnfinish.unfinish_aims.map((item)=>
       item._source)
      })
    }else{
    axios({
      method:"GET",
      url:'http://localhost:8901/app/data?type=users&&data=unfinish_aims&&username='+this.state.username,
      data:[]
        }).then(function(result) {
          let aimsdata=result.data.map((item)=>
         item._source);
     self.setState({
      unfinish_data:aimsdata
          });
       }).catch(function(err){
         console.log(err);
       });
    }
    axios({
      method:"GET",
      url:'http://localhost:8901/app/fields?id=unfinish_aims',
      data:[]
        }).then(function(result) {
          self.setState({
            unfinish_columns:result.data.columns
                });
       }).catch(function(err){
         console.log(err);
       });
}
  refreshData(){
    let self=this;
    let username = this.state.username;
    axios({
      method:"GET",
      url:'http://localhost:8901/app/data?type=users&&data=unfinish_aims&&username='+username,
        }).then(function(result) {
        let tasksdata=result.data.map((item)=>
          item._source);
      self.setState({
       unfinish_data:tasksdata
           });
       }).catch(function(err){
         console.log(err);
       });

  }
  render(){
    function Displaytime(columns){
         if(columns.key!=='reservation'){
             return (text,record)=>(
                  <span>
                  {new Date(text).toLocaleString()}
                  </span>
              );
           }else{
             return (text,record)=>(
                  <span>
                  {new Date(text[0]).toLocaleDateString()+'~'+new Date(text[1]).toLocaleDateString()}
                  </span>
              );
           }
         }
    function DispAimStatus(columns){
     let task_state = {"0":"启动","1":"归档"};
     let aim_approve_state = {"0":"待审批","1":"通过","2":"拒绝"};
     let aim_option = {"0":"核查","1":"中标","2":"资源申请"};
     let aim_view = {"0":"只显号码","1":"只显内容","2":"全部显示"};

     switch (columns.key) {
     case 'aim_state':
      return (text,record)=>( <span>{task_state[text]} </span>);
       //break;
     case 'aim_approve_state':
      return (text,record)=>(<span>{aim_approve_state[text]} </span>);
      //break;
    case 'aim_option':
     return (text,record)=>( <span>{aim_option[text]} </span>);
      //break;
    case 'aim_view':
     return (text,record)=>(<span>{aim_view[text]} </span>);
     //break;
     default:
      return (text,record)=>(<span>{text}</span>);
          }
        }
    let unfinish_columns = this.state.unfinish_columns.slice();
    let username = this.state.username;
    for(var i in unfinish_columns){
      if(unfinish_columns[i].type==="action"){
        unfinish_columns[i]={
          title:unfinish_columns[i].cn,
          key:unfinish_columns[i].key,
          dataIndex:unfinish_columns[i].key,
          display:unfinish_columns[i].display,
          render: (text, record) => ( <div>
              <Commonmodal
              key = "edit"
              name = "编辑"
              username = {username}
              display = {unfinish_columns}
              record = {record}
              refreshData = {this.refreshData}
              dataname = "unfinish_aims" / >
              <Divider
              key = "divider"
              type = "vertical" / >
              <Deletemodal
              key = "delete"
              name = "删除"
              username = {username}
              record = {record}
              refreshData = {this.refreshData}
              dataname = "unfinish_aims"/ >
              </div>)
        };
      }else if (unfinish_columns[i].type === "time") {
        unfinish_columns[i] = {
          title: unfinish_columns[i].cn,
          key: unfinish_columns[i].key,
          dataIndex: unfinish_columns[i].key,
          display: unfinish_columns[i].display,
          render: Displaytime(unfinish_columns[i])
        };
      }else if (unfinish_columns[i].type === "num") {
        unfinish_columns[i] = {
          title: unfinish_columns[i].cn,
          key: unfinish_columns[i].key,
          dataIndex: unfinish_columns[i].key,
          display: unfinish_columns[i].display,
          render: DispAimStatus(unfinish_columns[i])
        };
      }else{
        unfinish_columns[i]={
          title:unfinish_columns[i].cn,
          key:unfinish_columns[i].key,
          dataIndex:unfinish_columns[i].key,
          display:unfinish_columns[i].display,
          width:unfinish_columns[i].width
        };
      }
    }

    return(
      <Layout>
      <MainPage crumb={[<Link to={'/layout/task/statistic'}>案件管理</Link>,"待审批目标"]}>
      <CommonTables
      title="待审批目标"
      columns={unfinish_columns}
      data={this.state.unfinish_data}
      tabletitle={() =><Addmodal name="新增" username={username} display={unfinish_columns} dataname="aims" refreshData={this.refreshData}/>}
      />
      </MainPage>
      </Layout>
    );
  }
}

export default connect(
  state=>({UserInfo:state.UserInfo,AimsUnfinish:state.AimsUnfinish})
)(AimUnfinish);
