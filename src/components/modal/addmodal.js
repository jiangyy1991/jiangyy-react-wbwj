import React from 'react';
import FormList from './../form/formlist';
import {Form, Modal, Button} from 'antd';
import 'antd/dist/antd.css';
import axios from 'axios';
import Swal from 'sweetalert';

class  Addmodal extends React.Component{
  constructor(props){
    super(props);
    this.state={
      visible:false,
    }
    this.showModal = this.showModal.bind(this);
    this.handleOk = this.handleOk.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  showModal(){
    this.setState({
      visible:true,
    });
  }

  handleOk(){
    const { getFieldsValue, resetFields } = this.props.form;
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
    var username = this.props.username;
    const values = getFieldsValue();
    var myDate = new Date();
    var uuid = "wbwj"+myDate.getFullYear()+myDate.getMonth()+myDate.getDate()+myDate.getHours()+myDate.getMinutes()+myDate.getSeconds()+myDate.getMilliseconds();
    //重置表单 (坑点)
    //此处发送ajax请求，向后台写数据
    values.key = uuid;
    if(this.props.dataname==='tasks'){
      values.task_create=username;
      values.task_state="0";
      values.task_approve_state="0";
      values.task_edittime=new Date().getTime();
    }else if(this.props.dataname==='aims'){
      values.aim_create=username;
      values.aim_approve_state="0";
      values.aim_edittime=new Date().getTime();
    }else{
      values.state='0';
    }
    const refreshData = this.props.refreshData;
    let dataname = this.props.dataname;
    let dataaims={users:values.account,tasks:values.task_name,aims:values.aim_name};

    axios({
      method:"POST",
      url:'http://localhost:8901/app/data',
      datatype:'json',
      data:{dataname:this.props.dataname,data:values}
        }).then(function(result) {
      Swal("新建成功！").then(function(){
          refreshData();
          let datatype={users:'用户',tasks:'案件',aims:'目标'};
          //console.log(datatype['users']);
          axios({
            method:"POST",
            url:'http://localhost:8901/app/logstash',
            datatype:'json',
            data:{data:{user:username,action:'新增',time:new Date().getTime(),desp:'新增'+datatype[dataname]+dataaims[dataname]}}
              }).then(function(result) {
            console.log("插入日志");
          }).catch(function(err){
               console.log(err);
             });
          }
        )
       }).catch(function(err){
         console.log(err);
       });
    resetFields();
    this.setState({
      visible:false,
    });
  }
});
  }
  handleCancel(){
    const {resetFields}= this.props.form;
    resetFields();
    this.setState({
      visible:false,
    });
  }

  render(){
    let displays = this.props.display.slice();
    let disabled=[];
    for(var i in displays){
      if(displays[i].display!=='none'){
        disabled[displays[i].key]=false;
      }
    }
    return(
      <span>
      <Button type="primary" onClick={this.showModal}>{this.props.name}</Button>
      <Modal
      title={this.props.name}
      visible={this.state.visible}
      onOk={this.handleOk}
      onCancel={this.handleCancel}
      okText="确认"
      cancelText="取消"
      >
      <FormList disabled={disabled} form={this.props.form} display={this.props.display}/>
      </Modal>
      </span>
    );
  }
}

export default Form.create()(Addmodal);
