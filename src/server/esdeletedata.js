const elasticsearch = require('elasticsearch');

module.exports = function(querystr,callback){
   let querystring = querystr.dataname.split('_');
   if(querystring.length!=1){
     querystr.dataname=querystring[1];
   }
  const esClient_post = new elasticsearch.Client({
    host:'localhost:9200',
    log:'error'
});
  esClient_post.deleteByQuery({
    index:querystr.dataname,
    body:{
    query:{
      term:{key:querystr.key}
    }}
  }).then(function(re){
    callback(re);
  },function(err){
    console.trace(err.message);
  });

}
