export function AimsUnfinish(state = {unfinish_aims:null}, action){
  switch (action.type) {
    case 'UnfinishAimsData':
      return({
          unfinish_aims:action.unfinish_aims
        })
    default:
      return state
  }
}

export function AimUnfinish(unfinish_aims) {
  return {type:'UnfinishAimsData',unfinish_aims:unfinish_aims}
}
