import React from 'react';
import axios from 'axios';
import {Layout} from 'antd';
import {connect} from 'react-redux';
import ApproveSider from './sider';
import MainSider from './../../components/sider/mainsider';
import MainPage from './../../components/content/mainpage';
import CommonTables from './../../components/tables';
import Approvemodal from './../../components/modal/approvemodal';
import {Link} from 'react-router-dom';

class PendingList extends React.Component{
  constructor(props){
    super(props);
    this.state={
      username:null,
      unfinish_tasks:[],
      tasks_columns:[],
      unfinish_aims:[],
      aims_columns:[]
    }
    this.refreshData = this.refreshData.bind(this);
  }
  handleClick = (e) => {
    console.log('click ', e);
  }
  componentWillMount() {
    var userinfo = sessionStorage.getItem('userinfo');
      if (this.props.UserInfo.account===null) {
        this.setState({
          username: JSON.parse(userinfo).account
        });
      }else{
      this.setState({
        username: this.props.account
      });
    }
  }
  componentDidMount(){
    let self = this;
    if(this.props.ApproveTaskUnfinish.unfinish_tasks!==null){
      this.setState({
        unfinish_tasks:this.props.ApproveTaskUnfinish.unfinish_tasks.map((item)=>
       item._source)
      })
    }else{
    axios({
      method:"GET",
      url:'http://localhost:8901/app/data?type=approve&&data=unfinish_tasks&username='+this.state.username,
      data:[]
        }).then(function(result) {
          let tasksdata=result.data.map((item)=>
         item._source);
     self.setState({
      unfinish_tasks:tasksdata
          });
       }).catch(function(err){
         console.log(err);
       });
    }
    axios({
      method:"GET",
      url:'http://localhost:8901/app/fields?id=unfinish_tasks_approve',
      data:[]
        }).then(function(result) {
          self.setState({
            tasks_columns:result.data.columns
                });
       }).catch(function(err){
         console.log(err);
       });
       if(this.props.ApproveAimsUnfinish.unfinish_aims!==null){
         this.setState({
           unfinish_aims:this.props.ApproveAimsUnfinish.unfinish_aims.map((item)=>
          item._source)
         })
       }else{
    axios({
      method:"GET",
      url:'http://localhost:8901/app/data?type=approve&&data=unfinish_aims&username='+this.state.username,
      data:[]
        }).then(function(result) {
          let aimsdata=result.data.map((item)=>
         item._source);
         self.setState({
             unfinish_aims:aimsdata
                 });
        }).catch(function(err){
        console.log(err);
      });
    }
   axios({
     method:"GET",
     url:'http://localhost:8901/app/fields?id=unfinish_aims_approve',
     data:[]
       }).then(function(result) {
         self.setState({
           aims_columns:result.data.columns
               });
      }).catch(function(err){
        console.log(err);
      });
  }
  refreshData(){
    let self=this;
    let username = this.state.username;
    axios({
      method:"GET",
      url:'http://localhost:8901/app/data?type=approve&&data=unfinish_tasks&username='+username,
        }).then(function(result) {
        let tasksdata=result.data.map((item)=>
          item._source);
      self.setState({
       unfinish_tasks:tasksdata
           });
       }).catch(function(err){
         console.log(err);
       });
   axios({
     method:"GET",
     url:'http://localhost:8901/app/data?type=approve&&data=unfinish_aims&username='+username,
       }).then(function(result) {
       let tasksdata=result.data.map((item)=>
         item._source);
     self.setState({
      unfinish_aims:tasksdata
          });
      }).catch(function(err){
        console.log(err);
      });

  }
  render(){
    function Displaytime(columns){
         if(columns.key!=='reservation'){
             return (text,record)=>(
                  <span>
                  {new Date(text).toLocaleString()}
                  </span>
              );
           }else{
             return (text,record)=>(
                  <span>
                  {new Date(text[0]).toLocaleDateString()+'~'+new Date(text[1]).toLocaleDateString()}
                  </span>
              );
           }
         }
   function DispTaskOther(columns){
      switch (columns.key) {
        case 'task_member':
         return (text,record)=>( <span>{text.join(' | ')} </span>);
        //  break;
        default:
         return (text,record)=>(<span>{text}</span>);
      }
    }
    function DispAimStatus(columns){
      let task_state = {"0":"启动","1":"归档"};
      let aim_approve_state = {"0":"待审批","1":"通过","2":"拒绝"};
      let aim_option = {"0":"核查","1":"中标","2":"资源申请"};
      let aim_view = {"0":"只显号码","1":"只显内容","2":"全部显示"};

      switch (columns.key) {
      case 'aim_state':
       return (text,record)=>( <span>{task_state[text]} </span>);
      //  break;
      case 'aim_approve_state':
       return (text,record)=>(<span>{aim_approve_state[text]} </span>);
      // break;
     case 'aim_option':
      return (text,record)=>( <span>{aim_option[text]} </span>);
      // break;
     case 'aim_view':
      return (text,record)=>(<span>{aim_view[text]} </span>);
      //break;
      default:
       return (text,record)=>(<span>{text}</span>);
           }
         }
    let tasks_columns = this.state.tasks_columns.slice();
    let aims_columns = this.state.aims_columns.slice();
    let username = this.state.username;
    for(var i in tasks_columns){
      if(tasks_columns[i].type==="action"){
        tasks_columns[i]={
          title:tasks_columns[i].cn,
          key:tasks_columns[i].key,
          dataIndex:tasks_columns[i].key,
          display:tasks_columns[i].display,
          render: (text, record) => ( <div>
              <Approvemodal
              key = "approve"
              name = "审批"
              username = {username}
              display = {tasks_columns}
              record = {record}
              refreshData = {this.refreshData}
              dataname = "unfinish_tasks_approve" / >
              </div>)
        };
      }else if(tasks_columns[i].type==='time'){
        tasks_columns[i]={
          title:tasks_columns[i].cn,
          key:tasks_columns[i].key,
          dataIndex:tasks_columns[i].key,
          display:tasks_columns[i].display,
          width:tasks_columns[i].width,
          render:Displaytime(tasks_columns[i])
        };
      }else if(tasks_columns[i].type==='other'){
        tasks_columns[i]={
          title:tasks_columns[i].cn,
          key:tasks_columns[i].key,
          dataIndex:tasks_columns[i].key,
          display:tasks_columns[i].display,
          width:tasks_columns[i].width,
          render:DispTaskOther(tasks_columns[i])
        };
      }else{
        tasks_columns[i]={
          title:tasks_columns[i].cn,
          key:tasks_columns[i].key,
          dataIndex:tasks_columns[i].key,
          display:tasks_columns[i].display,
          width:tasks_columns[i].width
        };
      }
    }
    for(var j in aims_columns){
      if(aims_columns[j].type==="action"){
        aims_columns[j]={
          title:aims_columns[j].cn,
          key:aims_columns[j].key,
          dataIndex:aims_columns[j].key,
          display:aims_columns[j].display,
          render: (text, record) => ( <div>
              <Approvemodal
              key = "approve"
              name = "审批"
              username = {username}
              display = {aims_columns}
              record = {record}
              refreshData = {this.refreshData}
              dataname = "unfinish_aims_approve" / >
              </div>)
        };
      }else if(aims_columns[j].type==='time'){
        aims_columns[j]={
          title:aims_columns[j].cn,
          key:aims_columns[j].key,
          dataIndex:aims_columns[j].key,
          display:aims_columns[j].display,
          width:aims_columns[j].width,
          render:Displaytime(aims_columns[j])
        };
      }else if(aims_columns[j].type==='num'){
        aims_columns[j]={
          title:aims_columns[j].cn,
          key:aims_columns[j].key,
          dataIndex:aims_columns[j].key,
          display:aims_columns[j].display,
          width:aims_columns[j].width,
          render:DispAimStatus(aims_columns[j])
        };
      }else{
        aims_columns[j]={
          title:aims_columns[j].cn,
          key:aims_columns[j].key,
          dataIndex:aims_columns[j].key,
          display:aims_columns[j].display,
          width:aims_columns[j].width
        };
      }
    }

    return(
      <Layout>
      <MainSider>
      <ApproveSider selectkey={"pending"} />
      </MainSider>
      <MainPage  crumb={[<Link to={'/layout/approve/statistic'}>审批管理</Link>,"待审批列表"]}>
      <CommonTables
      title="待审批案件"
      columns={tasks_columns}
      data={this.state.unfinish_tasks}
      tabletitle={() =><div></div>}
      />
      <br />
      <CommonTables
      title="待审批目标"
      username = {username}
      columns={aims_columns}
      data={this.state.unfinish_aims}
      tabletitle={() =><div></div>}
      />
      </MainPage>
      </Layout>
    );
  }
}

export default connect(
  state=>({UserInfo:state.UserInfo,ApproveTaskUnfinish:state.ApproveTaskUnfinish,ApproveAimsUnfinish:state.ApproveAimsUnfinish})
)(PendingList);
