import React from 'react';
import {Link} from 'react-router-dom';
import {Menu} from 'antd';

const SubMenu = Menu.SubMenu;

class ApproveSider extends React.Component {

  render(){
    return(
      <Menu
        onClick={this.handleClick}
        defaultSelectedKeys={[this.props.selectkey]}
        defaultOpenKeys={['approve']}
        mode="inline"
      >
        <SubMenu key="approve" title={<span>审批管理</span>}>
          <Menu.Item key="statistic"><Link to={'/layout/approve/statistic'}>审批统计</Link></Menu.Item>
          <Menu.Item key="pending"><Link to={'/layout/approve/pendinglist'}>待审批列表</Link></Menu.Item>
          <Menu.Item key="tasks"><Link to={'/layout/approve/tasklist'}>案件审批</Link></Menu.Item>
          <Menu.Item key="aims"><Link to={'/layout/approve/aimlist'}>目标审批</Link></Menu.Item>
        </SubMenu>
      </Menu>
    )
  }
}

export default ApproveSider;
