import React from 'react';
import {Form, Input, DatePicker, Select, Radio} from 'antd';
import 'antd/dist/antd.css';
import moment from 'moment';
import axios from 'axios';
const RangePicker = DatePicker.RangePicker;
const FormItem = Form.Item;
const Option = Select.Option;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const formItemLayout = {
  labelCol:{
    xs:{span:24},
    sm:{span:5},
  },
  wrapperCol:{
    xs:{span:24},
    sm:{span:16}
  }
}

class  FormList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      approvers:[],
      members:[],
      contentfors:[],
      namesdata:[],
      taskname:[]
    }
    this.validateName = this.validateName.bind(this);
    this.validateTaskName = this.validateTaskName.bind(this);
  }

  componentDidMount(){
    let self = this;
    if(sessionStorage.getItem('userinfo')!=='undefined' && sessionStorage.getItem('userinfo')!==null){
      var userinfo = JSON.parse(sessionStorage.getItem('userinfo'));
      var username = userinfo.account;
    }
   axios({
     method:"GET",
     url:'http://localhost:8901/app/users?roles=1',
     data:[]
       }).then(function(result) {
       let tasksdata=result.data.map((item)=>
         item._source.account);
     self.setState({
      approvers:tasksdata
          });
      }).catch(function(err){
        console.log(err);
      });
      axios({
        method:"GET",
        url:'http://localhost:8901/app/users?roles=0',
        data:[]
          }).then(function(result) {
          let tasksdata=result.data.map((item)=>
            item._source.account);
            console.log(tasksdata);
        self.setState({
         members:tasksdata
             });
         }).catch(function(err){
           console.log(err);
         });
      axios({
         method:"GET",
         url:'http://localhost:8901/app/contentfors?account='+username,
         data:[]
           }).then(function(result) {
           let tasksdata=result.data.map((item)=>
             item._source.task_name);
             console.log(tasksdata);
         self.setState({
          contentfors:tasksdata
              });
          }).catch(function(err){
            console.log(err);
          });
      axios({
        method:"GET",
        url:'http://localhost:8901/app/users?roles='+["0","1","2","3"],
        data:[]
          }).then(function(result) {
          let namesdata=result.data.map((item)=>
            item._source.account);
        self.setState({
         namesdata:namesdata
             });
         }).catch(function(err){
           console.log(err);
         });
      axios({
         method:"GET",
         url:'http://localhost:8901/app/taskname',
         data:[]
         }).then(function(result) {
        let taskname=result.data.map((item)=>
            item._source.task_name);
        self.setState({
          taskname:taskname
              });
          }).catch(function(err){
            console.log(err);
          });
  }

  validateName = (rule, value, callback) => {
      //const form = this.props.form;
      if (this.state.namesdata.indexOf(value)!==-1) {
        if(this.props.record===undefined){
          callback('该用户名已被使用，请重新键入您的用户名');
        }else if(this.props.record.account!==value){
          callback('该用户名已被使用，请重新键入您的用户名');
        }else{
          callback();
        }
      }else{
        callback();
      }
    }

  validateTaskName = (rule, value, callback) => {
      //  const form = this.props.form;
        if (this.state.taskname.indexOf(value)!==-1) {
          if(this.props.record===undefined){
            callback('该任务名称已被使用，请重新输入');
          }else if(this.props.record.task_name!==value){
            callback('该任务名称已被使用，请重新输入');
          }else{
            callback();
          }
        }else{
          callback();
        }
      }
  render(){
    const { getFieldDecorator } = this.props.form;
    let listItems=this.props.display;
    let record = this.props.record?this.props.record:[];
    let disabled= this.props.disabled;
    let validateName = this.validateName;
    let validateTaskName = this.validateTaskName;
    function itemdisplay(listItems,record,disabled,state){
      if(listItems.display==="input"&&listItems.dataIndex==="account"){
        return (
          <FormItem key={listItems.dataIndex}
          {...formItemLayout}
          label={listItems.title} >
          {getFieldDecorator(listItems.dataIndex, {
              initialValue:record[listItems.dataIndex],
              rules: [{
                required: true, message: '请正确填入内容!',
              }, {
                validator: validateName,
              }],
            })(<Input disabled={disabled[listItems.dataIndex]}/>)}
          </FormItem>);
        }else if(listItems.display==="input"&&listItems.dataIndex==="task_name"){
          return (
            <FormItem key={listItems.dataIndex}
            {...formItemLayout}
            label={listItems.title} >
            {getFieldDecorator(listItems.dataIndex, {
                initialValue:record[listItems.dataIndex],
                rules: [{
                  required: true, message: '请正确填入内容!',
                }, {
                  validator: validateTaskName,
                }],
              })(<Input disabled={disabled[listItems.dataIndex]}/>)}
            </FormItem>);
          }else if(listItems.display==="input"){
          return (
            <FormItem key={listItems.dataIndex}
            {...formItemLayout}
            label={listItems.title} >
            {getFieldDecorator(listItems.dataIndex, {
                initialValue:record[listItems.dataIndex],
                rules: [{
                  required: true, message: '请正确填入内容!',
                }],
              })(<Input disabled={disabled[listItems.dataIndex]}/>)}
            </FormItem>);
           }else if(listItems.display==="time"){
           var displaytime = record[listItems.dataIndex]?record[listItems.dataIndex].map((item)=>moment(item,"YYYY-MM-DD")):[];
        return (
          <FormItem key={listItems.dataIndex}
          {...formItemLayout}
          label={listItems.title} >
          {getFieldDecorator(listItems.dataIndex, {
              initialValue:displaytime,
              rules: [{
                required: true, message: '请正确填入内容!',
              }],
            })(<RangePicker format="YYYY-MM-DD" placeholder={['开始时间','结束时间']} disabled={disabled[listItems.dataIndex]} />)}
          </FormItem>);
        }else if(listItems.display==="select"&&listItems.dataIndex==="task_approver"){
        return (
          <FormItem key={listItems.dataIndex}
          {...formItemLayout}
          label={listItems.title} >
          {getFieldDecorator(listItems.dataIndex, {
              initialValue:record[listItems.dataIndex],
              rules: [{
                required: true, message: '请正确填入内容!',
              }],
            })(<Select disabled={disabled[listItems.dataIndex]}>
              {state.approvers.map((approver)=>
                 <Option key={approver} value={approver}>{approver}</Option>
              )}
          </Select>)}
          </FormItem>);
        }else if(listItems.display==="select"&&listItems.dataIndex==="task_resp"){
        return (
          <FormItem key={listItems.dataIndex}
          {...formItemLayout}
          label={listItems.title} >
          {getFieldDecorator(listItems.dataIndex, {
              initialValue:record[listItems.dataIndex],
              rules: [{
                required: true, message: '请正确填入内容!',
              }],
            })(<Select disabled={disabled[listItems.dataIndex]}>
              {state.members.map((member)=>
                 <Option key={member} value={member}>{member}</Option>
              )}
          </Select>)}
          </FormItem>);
        }else if(listItems.display==="select"&&listItems.dataIndex==="aim_for"){
        return (
          <FormItem key={listItems.dataIndex}
          {...formItemLayout}
          label={listItems.title} >
          {getFieldDecorator(listItems.dataIndex, {
              initialValue:record[listItems.dataIndex],
              rules: [{
                required: true, message: '请正确填入内容!',
              }],
            })(<Select disabled={disabled[listItems.dataIndex]}>
            {state.contentfors.map((contentfor)=>
               <Option key={contentfor} value={contentfor}>{contentfor}</Option>
            )}
          </Select>)}
          </FormItem>);
        }else if(listItems.display==="multiple"&&listItems.dataIndex==='roles'){
        return (
          <FormItem key={listItems.dataIndex}
          {...formItemLayout}
          label={listItems.title} >
          {getFieldDecorator(listItems.dataIndex, {
              initialValue:record[listItems.dataIndex],
              rules: [{
                required: true, message: '请正确填入内容!',
              }],
            })(<Select mode='multiple' disabled={disabled[listItems.dataIndex]}>
           <Option value="0">业务员</Option>
           <Option value="1">审批员</Option>
           <Option value="2">用户管理员</Option>
           <Option value="3">日志管理员</Option>
          </Select>)}
          </FormItem>);
        }else if(listItems.display==="multiple"&&listItems.dataIndex==='task_member'){
        return (
          <FormItem key={listItems.dataIndex}
          {...formItemLayout}
          label={listItems.title} >
          {getFieldDecorator(listItems.dataIndex, {
              initialValue:record[listItems.dataIndex],
              rules: [{
                required: true, message: '请正确填入内容!',
              }],
            })(<Select mode='multiple' disabled={disabled[listItems.dataIndex]}>
          {state.members.map((member)=>
             <Option key={member} value={member}>{member}</Option>
          )}
          </Select>)}
          </FormItem>);
        }else if(listItems.display==="radio"&&listItems.dataIndex==='aim_view'){
          return (
            <FormItem key={listItems.dataIndex}
            {...formItemLayout}
            label={listItems.title} >
            {getFieldDecorator(listItems.dataIndex, {
                initialValue:record[listItems.dataIndex],
                rules: [{
                  required: true, message: '请正确填入内容!',
                }],
              })(<RadioGroup disabled={disabled[listItems.dataIndex]}>
             <RadioButton value="0">只显号码</RadioButton>
             <RadioButton value="1">只显内容</RadioButton>
             <RadioButton value="2">全部显示</RadioButton>
            </RadioGroup>)}
            </FormItem>);
        }else if(listItems.display==="radio"&&listItems.dataIndex==='aim_option'){
          return (
            <FormItem key={listItems.dataIndex}
            {...formItemLayout}
            label={listItems.title} >
            {getFieldDecorator(listItems.dataIndex, {
                initialValue:record[listItems.dataIndex],
                rules: [{
                  required: true, message: '请正确填入内容!',
                }],
              })(<RadioGroup disabled={disabled[listItems.dataIndex]}>
             <RadioButton value="0">核查</RadioButton>
             <RadioButton value="1">中标</RadioButton>
             <RadioButton value="2">资源申请</RadioButton>
            </RadioGroup>)}
            </FormItem>);
        }else if(listItems.display==="radio"&&(listItems.dataIndex==='task_approve_state'||listItems.dataIndex==='aim_approve_state')){
          return (
            <FormItem key={listItems.dataIndex}
            {...formItemLayout}
            label={listItems.title} >
            {getFieldDecorator(listItems.dataIndex, {
                initialValue:record[listItems.dataIndex],
                rules: [{
                  required: true, message: '请正确填入内容!',
                }],
              })(<RadioGroup disabled={disabled[listItems.dataIndex]}>
             <RadioButton value="0">待审批</RadioButton>
             <RadioButton value="1">通过</RadioButton>
             <RadioButton value="2">拒绝</RadioButton>
            </RadioGroup>)}
            </FormItem>);
        }else {
             return;
           }
    }
    return(
      <Form>
     {listItems.map((item)=>
     itemdisplay(item,record,disabled,this.state)
   )}
   </Form>
   )
  }
}


export default Form.create()(FormList);
