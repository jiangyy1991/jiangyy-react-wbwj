import React from 'react';
import { Modal, Button, Form,Input} from 'antd';
import 'antd/dist/antd.css';
import axios from 'axios';
import Swal from 'sweetalert';
const FormItem = Form.Item;

class  ChangePass extends React.Component{
  constructor(props){
    super(props);
    this.state={
      visible:false
    }
    this.showModal = this.showModal.bind(this);
    this.handleOk = this.handleOk.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleConfirmBlur = this.handleConfirmBlur.bind(this);
    this.compareToFirstPassword = this.compareToFirstPassword.bind(this);
    this.validateToNextPassword =  this.validateToNextPassword.bind(this);
  }

  showModal(){
    this.setState({
      visible:true,
    });
  }
  handleConfirmBlur = (e) => {
      const value = e.target.value;
      this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    }
  compareToFirstPassword = (rule, value, callback) => {
      const form = this.props.form;
      if (value && value !== form.getFieldValue('password')) {
        callback('请确认您的密码是否输入相同!');
      } else {
        callback();
      }
    }
  validateToNextPassword = (rule, value, callback) => {
      const form = this.props.form;
      if (value && this.state.confirmDirty) {
        form.validateFields(['confirm'], { force: true });
      }
      callback();
    }

  handleOk(){
    let self = this;
    //发送ajax请求,删除某条数据
    //const { getFieldsValue, resetFields } = this.props.form;
    const { resetFields } = this.props.form;
    this.props.form.validateFieldsAndScroll((err, values) => {
    if (!err) {
    var username = this.props.username;
    let data = {password:values.password}
      axios({
        method:"POST",
        url:'http://localhost:8901/app/changepass',
        datatype:'json',
        data:{data:data,username:username,password:values.password_old}
        }).then(function(result) {
          //console.log(result);
          if(result.data!=="400"){
          Swal("修改成功！").then(function(){
            resetFields();
            self.setState({
                visible:false,
                    });
            axios({
              method:"POST",
              url:'http://localhost:8901/app/logstash',
              datatype:'json',
              data:{data:{user:username,action:'修改密码',time:new Date().getTime(),desp:'修改登陆密码'}}
                }).then(function(result) {
              console.log("插入日志");
            }).catch(function(err){
                 console.log(err);
               });
                  }
                )
              }else{
                Swal({
                  text: "用户名或密码错误！",
                  buttons: {
                    cancel: false,
                    confirm: "确定"
                    }
                });
              }
               }).catch(function(err){
                 console.log(err);
               });
  }});
  }
  handleCancel(){
    this.setState({
      visible:false,
    });
  }
  render(){
    const {
      getFieldDecorator,
//      resetFields
    } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: {
          span: 24
        },
        sm: {
          span: 5
        },
      },
      wrapperCol: {
        xs: {
          span: 24
        },
        sm: {
          span: 16
        },
      },
    };
    return(
      <span>
      <Button type="primary" size="small" onClick={this.showModal}>修改密码</Button>
      <Modal
      title="修改密码"
      visible={this.state.visible}
      onOk={this.handleOk}
      onCancel={this.handleCancel}
      okText="确认"
      cancelText="取消"
      >
      <div>
      <Form>
      <FormItem
      { ...formItemLayout}
      label = "初始密码" >
      {
        getFieldDecorator('password_old', {
          rules: [{
            required: true,
            message: '请输入您的初始密码!',
          }, {
            validator: this.validateToNextPassword,
          }],
        })( <Input type = "password"  placeholder='请输入初始密码!'/ >
        )
      }
      </FormItem>
      <FormItem
      { ...formItemLayout}
      label = "新密码" >
      {
        getFieldDecorator('password', {
          rules: [{
            required: true,
            message: '请输入您的新密码!',
          }, {
            validator: this.validateToNextPassword,
          }],
        })( <Input type = "password"  placeholder='请输入新密码!'/ >
        )
      }
      </FormItem>
      <FormItem
      { ...formItemLayout}
      label = "确认密码" >
      {
        getFieldDecorator('confirm', {
          rules: [{
            required: true,
            message: '请再次输入您的新密码!',
          }, {
            validator: this.compareToFirstPassword,
          }],
        })( <Input type = "password"  placeholder='请再次输入新密码!' onBlur = {this.handleConfirmBlur}/>
        )
      }
      </FormItem>
      </Form>
      </div>
      </Modal>
      </span>
    );
  }
}

export default Form.create()(ChangePass);
