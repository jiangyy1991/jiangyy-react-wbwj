import React from 'react';
import {Layout} from 'antd';
import MainPage from './../../components/content/mainpage';


class NoFound extends React.Component{
  render(){
    return(
      <Layout>
      <MainPage crumb={['403']}>
      <div>您没有权限访问此页面，请输入其他地址！</div>
      </MainPage>
      </Layout>
    )
  }
}

export default NoFound;
