const elasticsearch = require('elasticsearch');

module.exports = function(querystr,callback){
  const esClient_post = new elasticsearch.Client({
    host:'localhost:9200',
    log:'error'
});

  esClient_post.search({
    index:"tasks",
    type:"wbwj",
    body:{
    query:{
      bool:{
        must:[{
          query_string:{
            query:querystr.account,
            fields:["task_resp","task_member"]
          }},
        {
          match_phrase:{
          task_state:0
        }},
        {
          match_phrase:{
          task_approve_state:1
        }}]}
    }},
    _source:"task_name",
  }).then(function(re){
    callback(re.hits.hits);
  },function(err){
    console.trace(err.message);
  });

}
