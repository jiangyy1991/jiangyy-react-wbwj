import React from 'react';
import ApproveSider from './sider';
import { Layout,Card,Row,Col } from 'antd';
import axios from 'axios';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import MainSider from './../../components/sider/mainsider';
import MainPage from './../../components/content/mainpage';
import {ApproveUnfinish} from './../../reducer/approve_task_unfinish';
import {ApproveFinish} from './../../reducer/approve_task_finish';
import {ApproveAimUnfinish} from './../../reducer/approve_aim_unfinish';
import {ApproveAimFinish} from './../../reducer/approve_aim_finish';

class ApproveStatistic extends React.Component{
  constructor(props){
    super(props);
    this.state={
      username:null,
      unfinish_tasks_count:0,
      finish_tasks_count:0,
      unfinish_aims_count:0,
      finish_aims_count:0
    }
  }
  handleClick = (e) => {
    console.log('click ', e);
  }
  componentWillMount() {
    var userinfo = sessionStorage.getItem('userinfo');
      if (this.props.UserInfo.account=== null) {
        this.setState({
          username: JSON.parse(userinfo).account
        });
      }else{
      this.setState({
        username: this.props.account
      });
    }
  }
  componentDidMount(){
  let self = this;
    axios({
      method:"GET",
      url:'http://localhost:8901/app/data?type=approve&data=finish_tasks&&username='+this.state.username,
      data:[]
        }).then(function(result) {
          self.setState({finish_tasks_count:result.data.length});
          self.props.dispatch(ApproveFinish(result.data));
       }).catch(function(err){
         console.log(err);
       });
   axios({
     method:"GET",
     url:'http://localhost:8901/app/data?type=approve&data=unfinish_tasks&&username='+this.state.username,
     data:[]
       }).then(function(result) {
         self.setState({unfinish_tasks_count:result.data.length});
         self.props.dispatch(ApproveUnfinish(result.data));
      }).catch(function(err){
        console.log(err);
      });
    axios({
      method:"GET",
      url:'http://localhost:8901/app/data?type=approve&data=finish_aims&&username='+this.state.username,
      data:[]
        }).then(function(result) {
          self.setState({finish_aims_count:result.data.length});
          self.props.dispatch(ApproveAimFinish(result.data));
       }).catch(function(err){
         console.log(err);
       });
   axios({
     method:"GET",
     url:'http://localhost:8901/app/data?type=approve&data=unfinish_aims&&username='+this.state.username,
     data:[]
       }).then(function(result) {
         self.setState({unfinish_aims_count:result.data.length});
         self.props.dispatch(ApproveAimUnfinish(result.data));
      }).catch(function(err){
        console.log(err);
      });
}
  render(){
    return(
      <Layout>
      <MainSider>
      <ApproveSider selectkey={"statistic"} />
      </MainSider>
      <MainPage crumb={["审批管理"]}>
      <div>
      <span style={{fontSize:'1.5em',color:'rgb(0, 0, 0, 0.85)',fontWeight:500}}><b>审批统计</b>--<small>待审批</small></span>
      <span style={{position:'absolute',margin: '0.8em 1em 1em 2em'}}>
      <Link to={'/layout/approve/pendinglist'}>待审批列表</Link>
      </span>
      </div>
      <br/>
      <Row gutter={16}>
      <Col span={6}>
      <Card title="案件">
        <p><Link to={'/layout/approve/pendinglist'}>待审批案件:{this.state.unfinish_tasks_count}</Link></p>
      </Card>
      </Col>
      <Col span={6}>
      <Card title="目标">
        <p><Link to={'/layout/approve/pendinglist'}>待审批目标:{this.state.unfinish_aims_count}</Link></p>
      </Card>
      </Col>
      </Row>
    <br />
      <div>
      <span style={{fontSize:'1.5em',color:'rgb(0, 0, 0, 0.85)',fontWeight:500}}><b>审批统计</b>--<small>已审批</small></span>
      <span style={{position:'absolute',margin: '0.8em 1em 1em 2em'}}>
      <Link to={'/layout/approve/tasklist'}>案件列表</Link>
      </span>
      <span style={{position:'absolute',margin: '0.8em 1em 1em 8em'}}>
      <Link to={'/layout/approve/aimlist'}>目标列表</Link>
      </span>
      </div>
      <br/>
      <Row gutter={16}>
      <Col span={6}>
      <Card title="案件">
        <p><Link to={'/layout/approve/tasklist'}>已审批案件:{this.state.finish_tasks_count}</Link></p>
      </Card>
      </Col>
      <Col span={6}>
      <Card title="目标">
        <p><Link to={'/layout/approve/aimlist'}>已审批目标:{this.state.finish_aims_count}</Link></p>
      </Card>
      </Col>
      </Row>
      </MainPage>
      </Layout>
    );
  }
}

export default connect(
  state=>({UserInfo:state.UserInfo})
)(ApproveStatistic);
