import React from 'react';
import FormList from './../form/formlist';
import {Form, Modal, Button} from 'antd';
import 'antd/dist/antd.css';

class  Viewmodal extends React.Component{
  constructor(props){
    super(props);
    this.state={
      visible:false
    }
    this.showModal = this.showModal.bind(this);
    this.handleOk = this.handleOk.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  showModal(){
    this.setState({
      visible:true,
    });
  }

  handleOk(){
    const { getFieldsValue, resetFields } = this.props.form;
    const values = getFieldsValue();
    //发送ajax请求。
    console.log(values)
    //重置表单 (坑点)
    resetFields();
    this.setState({
      visible:false,
    });
  }
  handleCancel(){
    this.setState({
      visible:false,
    });
  }

  render(){
    let displays = this.props.display.slice();
    let disabled=[];
    for(var i in displays){
      if(displays[i].display!=='none'){
        disabled[displays[i].key]=true;
      }
    }
    return(
      <span>
      <Button type="primary" onClick={this.showModal}>{this.props.name}</Button>
      <Modal
      title={this.props.name}
      visible={this.state.visible}
      onOk={this.handleOk}
      onCancel={this.handleCancel}
      okText="确认"
      cancelText="取消"
      >
      <FormList form={this.props.form} disabled={disabled} display={this.props.display} record={this.props.record}/>
      </Modal>
      </span>
    );
  }
}

export default Form.create()(Viewmodal);
