import React from 'react';
import axios from 'axios';
import {Layout} from 'antd';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import TaskSider from './sider';
import MainSider from './../../components/sider/mainsider';
import MainPage from './../../components/content/mainpage';
import CommonTables from './../../components/tables';
import Restoremodal from './../../components/modal/restoremodal';

class ArchivedList extends React.Component{
  constructor(props){
    super(props);
    this.state={
      data:[],
      columns:[],
      username:null
    }
    this.refreshData = this.refreshData.bind(this);
  }
  componentWillMount() {
    var userinfo = sessionStorage.getItem('userinfo');
      if (this.props.UserInfo.account===null) {
        this.setState({
          username: JSON.parse(userinfo).account
        });
      }else{
      this.setState({
        username: this.props.account
      });
    }
    }
    componentDidMount(){
    let self = this;
    if(this.props.TaskArchived.archived_tasks!==null){
      this.setState({
        data:this.props.TaskArchived.archived_tasks.map((item)=>
       item._source)
      })
    }else{
    axios({
      method:"GET",
      url:'http://localhost:8901/app/data?type=users&&data=archived_tasks&&username='+this.state.username,
      data:[]
        }).then(function(result) {
          let tasksdata=result.data.map((item)=>
         item._source);
     self.setState({
      data:tasksdata
          });
       }).catch(function(err){
         console.log(err);
       });
     }
   axios({
     method:"GET",
     url:'http://localhost:8901/app/fields?id=tasks',
     data:[]
       }).then(function(result) {
         self.setState({
           columns:result.data.columns
               });
      }).catch(function(err){
        console.log(err);
      });
  }
  refreshData(){
    let self=this;
    let username = this.state.username;
    axios({
      method:"GET",
      url:'http://localhost:8901/app/data?type=users&&data=archived_tasks&&username='+username,
        }).then(function(result) {
        let tasksdata=result.data.map((item)=>
          item._source);
      self.setState({
       data:tasksdata
           });
       }).catch(function(err){
         console.log(err);
       });

  }
  render(){
    function Displaytime(columns){
         if(columns.key!=='reservation'){
             return (text,record)=>(
                  <span>
                  {new Date(text).toLocaleString()}
                  </span>
              );
           }else{
             return (text,record)=>(
                  <span>
                  {new Date(text[0]).toLocaleDateString()+'~'+new Date(text[1]).toLocaleDateString()}
                  </span>
              );
           }
         }
    function DispTaskOther(columns){
      switch (columns.key) {
        case 'task_member':
         return (text,record)=>( <span>{text.join(' | ')} </span>);
        //  break;
        default:
         return (text,record)=>(<span>{text}</span>);
      }
    }
    function DispTaskStatus(columns){
     let task_state = {"0":"启动","1":"归档"};
     let task_approve_state = {"0":"待审批","1":"通过","2":"拒绝"};
        switch (columns.key) {
          case 'task_state':
           return (text,record)=>( <span>{task_state[text]} </span>);
          //  break;
          case 'task_approve_state':
           return (text,record)=>(<span>{task_approve_state[text]} </span>);
          // break;
          default:
           return (text,record)=>(<span>{text}</span>);
        }
      }
    let columns = this.state.columns.slice();
    let username = this.state.username;
    for(var i in columns){
      if(columns[i].type==="action"){
        columns[i]={
          title:columns[i].cn,
          key:columns[i].key,
          dataIndex:columns[i].key,
          display:columns[i].display,
          render: (text, record) => ( <div>
              <Restoremodal
              key = "restore"
              name = "恢复"
              username = {username}
              display = {columns}
              record = {record}
              refreshData = {this.refreshData}
              dataname = "finish_tasks" / >
              </div>)
            };
      }else if(columns[i].type==='time'){
        columns[i]={
          title:columns[i].cn,
          key:columns[i].key,
          dataIndex:columns[i].key,
          display:columns[i].display,
          width:columns[i].width,
          render:Displaytime(columns[i])
        };
      }else if(columns[i].type==='other'){
        columns[i]={
          title:columns[i].cn,
          key:columns[i].key,
          dataIndex:columns[i].key,
          display:columns[i].display,
          width:columns[i].width,
          render:DispTaskOther(columns[i])
        };
      }else if(columns[i].type==='num'){
        columns[i]={
          title:columns[i].cn,
          key:columns[i].key,
          dataIndex:columns[i].key,
          display:columns[i].display,
          width:columns[i].width,
          render:DispTaskStatus(columns[i])
        };
      }else{
        columns[i]={
          title:columns[i].cn,
          key:columns[i].key,
          dataIndex:columns[i].key,
          display:columns[i].display,
          width:columns[i].width
            };
      }
    }

    return(
      <Layout>
      <MainSider>
      <TaskSider selectkey={"archived"} openkey={["task"]}/>
      </MainSider>
      <MainPage crumb={[<Link to={'/layout/task/statistic'}>案件管理</Link>,"归档案件列表"]}>
      <CommonTables
      title="已归档案件"
      columns={columns}
      data={this.state.data}
      tabletitle={() =><div></div>}
      />
      </MainPage>
      </Layout>
    );
  }
}

export default connect(
  state=>({UserInfo:state.UserInfo,TaskArchived:state.TaskArchived})
)(ArchivedList);
