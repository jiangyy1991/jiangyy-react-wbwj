import React from 'react';
import axios from 'axios';
import {Divider,Layout} from 'antd';
import {connect} from 'react-redux';
import MainPage from './../../components/content/mainpage';
import CommonTables from './../../components/tables';
import Addmodal from './../../components/modal/addmodal';
import Commonmodal from './../../components/modal/commonmodal';
import Deletemodal from './../../components/modal/deletemodal';
import Archivemodal from './../../components/modal/archivemodal';
import Detail from './detail/detail';
import {Link} from 'react-router-dom';

class TaskManage extends React.Component{
  constructor(props){
    super(props);
    this.state={
      username:null,
      unfinish_data:[],
      unfinish_columns:[],
      finish_data:[],
      finish_columns:[]
    }
    this.refreshData = this.refreshData.bind(this);
  }
  componentWillMount() {
    var userinfo = sessionStorage.getItem('userinfo');
      if (this.props.UserInfo.account===null) {
        this.setState({
          username: JSON.parse(userinfo).account
        });
      }else{
      this.setState({
        username: this.props.UserInfo.account
      });
    }
    }
    componentDidMount(){
    let self = this;
    if(this.props.TaskUnfinish.unfinish_tasks!==null){
      this.setState({
        unfinish_data:this.props.TaskUnfinish.unfinish_tasks.map((item)=>
       item._source)
      })
    }else{
    axios({
      method:"GET",
      url:'http://localhost:8901/app/data?type=users&&data=unfinish_tasks&&username='+this.state.username,
      data:[]
        }).then(function(result) {
          let tasksdata=result.data.map((item)=>
         item._source);
     self.setState({
      unfinish_data:tasksdata
          });
       }).catch(function(err){
         console.log(err);
       });
    }
    axios({
      method:"GET",
      url:'http://localhost:8901/app/fields?id=unfinish_tasks',
      data:[]
        }).then(function(result) {
          self.setState({
            unfinish_columns:result.data.columns
                });
       }).catch(function(err){
         console.log(err);
       });
       if(this.props.TaskFinish.finish_tasks!==null){
         this.setState({
           finish_data:this.props.TaskFinish.finish_tasks.map((item)=>
          item._source)
         })
       }else{
    axios({
      method:"GET",
      url:'http://localhost:8901/app/data?type=users&&data=finish_tasks&&username='+this.state.username,
      data:[]
        }).then(function(result) {
          let tasksdata=result.data.map((item)=>
         item._source);
     self.setState({
      finish_data:tasksdata
          });
       }).catch(function(err){
         console.log(err);
       });
     }
    axios({
     method:"GET",
     url:'http://localhost:8901/app/fields?id=finish_tasks',
     data:[]
       }).then(function(result) {
         self.setState({
           finish_columns:result.data.columns
               });
      }).catch(function(err){
        console.log(err);
      });
}
  refreshData(){
    let self=this;
    let username = this.state.username;
    axios({
      method:"GET",
      url:'http://localhost:8901/app/data?type=users&&data=unfinish_tasks&&username='+username,
        }).then(function(result) {
        let tasksdata=result.data.map((item)=>
          item._source);
        self.setState({
         unfinish_data:tasksdata
             });
       }).catch(function(err){
         console.log(err);
       });
   axios({
     method:"GET",
     url:'http://localhost:8901/app/data?type=users&&data=finish_tasks&&username='+username,
       }).then(function(result) {
       let tasksdata=result.data.map((item)=>
         item._source);
       self.setState({
        finish_data:tasksdata
            });
      }).catch(function(err){
        console.log(err);
      });
  }

  render(){
    function Displaytime(columns){
         if(columns.key!=='reservation'){
             return (text,record)=>(
                  <span>
                  {new Date(text).toLocaleString()}
                  </span>
              );
           }else{
             return (text,record)=>(
                  <span>
                  {new Date(text[0]).toLocaleDateString()+'~'+new Date(text[1]).toLocaleDateString()}
                  </span>
              );
           }
         }
    function DispTaskOther(columns){
      switch (columns.key) {
        case 'task_member':
         return (text,record)=>( <span>{text.join(' | ')} </span>);
        //  break;
        default:
         return (text,record)=>(<span>{text}</span>);
      }
    }
    function DispTaskStatus(columns){
     let task_state = {"0":"启动","1":"归档"};
     let task_approve_state = {"0":"待审批","1":"通过","2":"拒绝"};

        switch (columns.key) {
          case 'task_state':
           return (text,record)=>( <span>{task_state[text]} </span>);
          //  break;
          case 'task_approve_state':
           return (text,record)=>(<span>{task_approve_state[text]} </span>);
          // break;
          default:
           return (text,record)=>(<span>{text}</span>);
        }
      }
    let unfinish_columns = this.state.unfinish_columns.slice();
    let finish_columns = this.state.finish_columns.slice();
    let username = this.state.username;
    for(var i in unfinish_columns){
      if(unfinish_columns[i].type==="action"){
        unfinish_columns[i]={
          title:unfinish_columns[i].cn,
          key:unfinish_columns[i].key,
          dataIndex:unfinish_columns[i].key,
          display:unfinish_columns[i].display,
          render: (text, record) => ( <div>
              <Commonmodal
              key = "edit"
              name = "编辑"
              username = {username}
              display = {unfinish_columns}
              record = {record}
              refreshData = {this.refreshData}
              dataname = "unfinish_tasks" / >
              <Divider
              key = "divider"
              type = "vertical" / >
              <Deletemodal
              key = "delete"
              name = "删除"
              username = {username}
              record = {record}
              refreshData = {this.refreshData}
              dataname = "unfinish_tasks"/ >
              </div>)
        };
      }else if(unfinish_columns[i].type==='time'){
        unfinish_columns[i]={
          title:unfinish_columns[i].cn,
          key:unfinish_columns[i].key,
          dataIndex:unfinish_columns[i].key,
          display:unfinish_columns[i].display,
          width:unfinish_columns[i].width,
          render:Displaytime(unfinish_columns[i])
        };
      }else if(unfinish_columns[i].type==='other'){
        unfinish_columns[i]={
          title:unfinish_columns[i].cn,
          key:unfinish_columns[i].key,
          dataIndex:unfinish_columns[i].key,
          display:unfinish_columns[i].display,
          width:unfinish_columns[i].width,
          render:DispTaskOther(unfinish_columns[i])
        };
      }else{
        unfinish_columns[i]={
          title:unfinish_columns[i].cn,
          key:unfinish_columns[i].key,
          dataIndex:unfinish_columns[i].key,
          display:unfinish_columns[i].display,
          width:unfinish_columns[i].width
        };
      }
    }
    for(var j in finish_columns){
      if(finish_columns[j].type==="action"){
        finish_columns[j]={
          title:finish_columns[j].cn,
          key:finish_columns[j].key,
          dataIndex:finish_columns[j].key,
          display:finish_columns[j].display,
          render: (text, record) => ( <div>
              <Detail data={record} username = {username} display={finish_columns}/>
              <Divider
              key = "divider"
              type = "vertical" / >
              <Archivemodal
              key = "archive"
              name = "归档"
              username = {username}
              record = {record}
              refreshData = {this.refreshData}
              dataname = "finish_tasks"/ >
              </div>)
        };
      }else if(finish_columns[j].type==='time'){
        finish_columns[j]={
          title:finish_columns[j].cn,
          key:finish_columns[j].key,
          dataIndex:finish_columns[j].key,
          display:finish_columns[j].display,
          width:finish_columns[j].width,
          render:Displaytime(finish_columns[j])
        };
      }else if(finish_columns[j].type==='other'){
        finish_columns[j]={
          title:finish_columns[j].cn,
          key:finish_columns[j].key,
          dataIndex:finish_columns[j].key,
          display:finish_columns[j].display,
          width:finish_columns[j].width,
          render:DispTaskOther(finish_columns[j])
        };
      }else if(finish_columns[j].type==='num'){
        finish_columns[j]={
          title:finish_columns[j].cn,
          key:finish_columns[j].key,
          dataIndex:finish_columns[j].key,
          display:finish_columns[j].display,
          width:finish_columns[j].width,
          render:DispTaskStatus(finish_columns[j])
        };
      }else{
        finish_columns[j]={
          title:finish_columns[j].cn,
          key:finish_columns[j].key,
          dataIndex:finish_columns[j].key,
          display:finish_columns[j].display,
          width:finish_columns[j].width
        };
      }
    }

    return(
      <Layout>
      <MainPage crumb={[<Link to={'/layout/task/statistic'}>案件管理</Link>,"案件管理"]}>
      <CommonTables
      title="待审批案件"
      columns={unfinish_columns}
      data={this.state.unfinish_data}
      tabletitle={() =><Addmodal name="新增" username={username} display={unfinish_columns} dataname="tasks" refreshData={this.refreshData}/>}
      />
      <br />
      <CommonTables
      title="已审批案件"
      columns={finish_columns}
      data={this.state.finish_data}
      tabletitle={() =><div></div>}
      />
      </MainPage>
      </Layout>
    );
  }
}

export default connect(
  state=>({UserInfo:state.UserInfo,TaskFinish:state.TaskFinish,TaskUnfinish:state.TaskUnfinish})
)(TaskManage);
