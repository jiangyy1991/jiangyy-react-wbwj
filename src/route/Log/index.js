import React from 'react';
import { Route ,Switch} from 'react-router-dom';
import LogManage from './logmanage';
import LogStatistic from './logstatistic';
import ViewList from './../Special/viewlist';

class  LogRoute extends React.Component{
  render(){
    return(
      <Switch>
      <Route exact path="/layout/log/statistic" component={LogStatistic} />
      <Route exact path="/layout/log/logmanage" component={LogManage} />
      <Route exact path="/layout/log/special" component={ViewList} />
      </Switch>
    )
  }
}

export default LogRoute;
