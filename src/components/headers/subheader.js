import React from 'react';
import {Layout, Button} from 'antd';
import {Link} from 'react-router-dom';
import ChangePass from './../modal/changepass';

const { Header} = Layout;

class SubHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      username:null
    }
  }
  componentWillMount() {
        var userinfo = sessionStorage.getItem('userinfo');
        if (userinfo !== null && userinfo !== 'undefined') {
         this.setState({
             username:JSON.parse(userinfo).account
           });
    }
  }
  render(){
    return(
      <Header style={{ background: '#fff', padding: 0 ,height:'40px',border:'solid 1px rgb(0,0,0)'}}>
      <div>
      <span style={{float:'left',margin:'-10px 0px 0px 30px'}}>
      {this.state.username},你好！
      </span>
      <span style={{float:'right' ,margin:'-12px 30px 0px 0px'}}>
      <ChangePass username={this.state.username}/>
      {" "}
      <Link to='/login'><Button type="primary" size="small" onClick={()=>sessionStorage.removeItem('userinfo')}>退出</Button></Link>
      </span>
      </div>
      </Header>
    )
  }
}

export default SubHeader;
