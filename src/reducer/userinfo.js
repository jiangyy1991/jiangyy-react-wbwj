export function UserInfo(state = {account:null}, action){
  switch (action.type) {
    case 'PutAccount':
      return({
          account : action.account
        })
    default:
      return state
  }
}

export function PutAccount(account) {
  return {type:'PutAccount',account:account}
}
