import React from 'react';


class IframesPage extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      iFrameHeight:'0px',
      link:null,
      keyword:null,
      timeinterval:[]
    }
    this.handleClick = this.handleClick.bind(this);
  }
  componentWillMount(){
    var data = JSON.parse(sessionStorage.getItem('recentview'));
    this.setState({
      keyword:data.aim_verify,
      timeinterval:data.reservation,
      link:data.link
    });
  }

  handleClick(){
    console.log(document.getElementsByTagName('iframe'));
    console.log(window.MutationObserver);
    console.log(window.webkitMutationObserver);
  }
  render() {
    return (
      <div>
      <button onClick={this.handleClick}>下载</button>
      <iframe style = {
        {
          width: '100%',
          height: this.state.iFrameHeight,
          overflow: 'visible'
        }
      }
      onLoad = {
        () => {
          this.setState({
            "iFrameHeight": '800px'
          });
        }
      }
      title={this.state.timeinterval}
      ref = "iframe"
      src = {this.state.link}
      width = "100%"
      height = {
        this.state.iFrameHeight
      }
      scrolling = "yes"
      frameBorder = "0"
      />
      </div>
    );
  }
}

export default IframesPage;
