import React from 'react';
import axios from 'axios';
import {
  Divider,
  Layout
} from 'antd';
import {connect} from 'react-redux';
import MainPage from './../../components/content/mainpage';
import CommonTables from './../../components/tables';
import Addmodal from './../../components/modal/addmodal';
import Commonmodal from './../../components/modal/commonmodal';
import Deletemodal from './../../components/modal/deletemodal';

class UserManage extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        data: [],
        columns: [],
        username:null
      }
      this.refreshData = this.refreshData.bind(this);
    }
  componentWillMount() {
    var userinfo = sessionStorage.getItem('userinfo');
      if (this.props.account==='null') {
        this.setState({
          username: JSON.parse(userinfo).account
        });
      }else{
      this.setState({
        username: this.props.account
      });
    }
      let self = this;
      axios({
        method: "GET",
        url: 'http://localhost:8901/app/data?type=all&&data=users',
        data: []
      }).then(function(result) {
        let tasksdata = result.data.map((item) =>
          item._source);
        self.setState({
          data: tasksdata
        });
      }).catch(function(err) {
        console.log(err);
      });
      axios({
        method: "GET",
        url: 'http://localhost:8901/app/fields?id=users',
        data: []
      }).then(function(result) {
        self.setState({
          columns: result.data.columns
        });
      }).catch(function(err) {
        console.log(err);
      });
    }

    refreshData() {
      let self = this;
      axios({
        method: "GET",
        url: 'http://localhost:8901/app/data?type=all&&data=users',
      }).then(function(result) {
        let tasksdata = result.data.map((item) =>
          item._source);
        self.setState({
          data: tasksdata
        });
      }).catch(function(err) {
        console.log(err);
      });

    }
    render() {
      let columns = this.state.columns.slice();
      let username = this.state.username;
      function DispUser(columns){
       let state = {"0":"启动","1":"停止"};
       let roles = {"0":"业务员","1":"审批员","2":"用户管理员","3":"日志管理员"};

          switch (columns.key) {
            case 'state':
             return (text,record)=>( <span>{state[text]} </span>);
            //  break;
            case 'roles':
             return (text,record)=>(<span>{text.map(item=>roles[item]).join(' | ')}</span>);
            // break;
            default:
             return (text,record)=>(<span>{text}</span>);
          }
        }
      for (var i in columns) {
        if (columns[i].type === "action") {
          columns[i] = {
            title: columns[i].cn,
            key: columns[i].key,
            dataIndex: columns[i].key,
            display: columns[i].display,
            render: (text, record) => ( <div>
                <Commonmodal
                key = "edit"
                name = "编辑"
                username = {username}
                display = {columns}
                record = {record}
                refreshData = {this.refreshData}
                dataname = "users" / >
                <Divider
                key = "divider"
                type = "vertical" / >
                <Deletemodal
                key = "delete"
                username = {username}
                name = "删除"
                record = {record}
                refreshData = {this.refreshData}
                dataname = "users" / >
                </div>)
          };
        } else if(columns[i].type==='num') {
          columns[i] = {
            title: columns[i].cn,
            key: columns[i].key,
            dataIndex: columns[i].key,
            display: columns[i].display,
            width: columns[i].width,
            render:DispUser(columns[i])
          };
        }else {
          columns[i] = {
            title: columns[i].cn,
            key: columns[i].key,
            dataIndex: columns[i].key,
            display: columns[i].display,
            width: columns[i].width
          };
        }
      }

      return (
        <Layout>
        <MainPage  crumb={["用户管理"]}>
        <CommonTables
        title = "用户管理"
        columns = {columns}
        data = {this.state.data}
        tabletitle = {() =>
          <Addmodal
          name = "新增"
          username= {username}
          display = {columns}
          dataname = "users"
          refreshData = {this.refreshData}/>}
          />
          </MainPage>
          </Layout>
        );
      }
    }

    export default connect(
      state=>state.UserInfo
    )(UserManage);
