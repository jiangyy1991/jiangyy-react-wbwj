import React from 'react';
import { Route, Redirect } from 'react-router-dom';

class AuthTask extends React.Component {
  render() {
    const { component: Component, ...rest } = this.props
    const isTask = JSON.parse(sessionStorage.getItem("userinfo")).roles.indexOf("0") === -1 ? true : false;
    return (
        <Route {...rest} render={props => {
          return isTask
              ? <Redirect to="/layout/403" />
              : <Component {...props} />
        }} />
    )
  }
}

export default AuthTask
