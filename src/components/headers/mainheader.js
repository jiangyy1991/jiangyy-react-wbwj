import React from 'react';
import { Layout, Menu} from 'antd';
import {Link} from 'react-router-dom';
const { Header} = Layout;
const SubMenu = Menu.SubMenu;

class MainHeader extends React.Component{
  constructor(props){
    super(props);
    this.state={
      userrole:[]
    }
  }
  componentWillMount(){
    if(sessionStorage.getItem('userinfo')!=='undefined' && sessionStorage.getItem('userinfo')!==null){
      var userinfo = JSON.parse(sessionStorage.getItem('userinfo'));
      this.setState({
        userroles : userinfo.roles
      });
    }
  }
  render(){
    let userroles = this.state.userroles;

    return(
      <Header className="header">
        <Menu
          theme="dark"
          mode="horizontal"
          style={{ float:'right' ,marginRight:'0px',lineHeight: '64px' }}
        >
        {
          userroles.indexOf('0')!==-1?(
         <SubMenu key="sub1" title={<span style={{fontSize:'18px'}}><b>案件管理</b></span>}>
          <Menu.Item key="taskview"><Link to={'/layout/task/statistic'}><span style={{fontSize:'15px'}}><b>案件查看</b></span></Link></Menu.Item>
          <Menu.Item key="taskmanage"><Link to={'/layout/task/taskmanage'}><span style={{fontSize:'15px'}}><b>案件管理</b></span></Link></Menu.Item>
          <Menu.Item key="aimmanage"><Link to={'/layout/task/aimmanage'}><span style={{fontSize:'15px'}}><b>目标管理</b></span></Link></Menu.Item>
         </SubMenu>):null
       }
       {
         userroles.indexOf('1')!==-1?(
           <SubMenu key="sub2" title={<span style={{fontSize:'18px'}}><b>审批管理</b></span>}>
          <Menu.Item key="approvemanage"><Link to={'/layout/approve/statistic'}><span style={{fontSize:'15px'}}><b>审批管理</b></span></Link></Menu.Item>
          </SubMenu>):null
        }
        {
          userroles.indexOf('2')!==-1?(
          <SubMenu key="sub3" title={<span style={{fontSize:'18px'}}><b>用户管理</b></span>}>
          <Menu.Item key="usermanage"><Link to={'/layout/manage/usermanage'}><span style={{fontSize:'15px'}}><b>用户管理</b></span></Link></Menu.Item>
          </SubMenu>):null
        }
        {
          userroles.indexOf('3')!==-1?(
          <SubMenu key="sub4" title={<span style={{fontSize:'18px'}}><b>日志管理</b></span>}>
          <Menu.Item key="logmanage"><Link to={'/layout/log/logmanage'}><span style={{fontSize:'15px'}}><b>日志管理</b></span></Link></Menu.Item>
          <Menu.Item key="logstastic"><Link to={'/layout/log/statistic'}><span style={{fontSize:'15px'}}><b>日志统计</b></span></Link></Menu.Item>
          <Menu.Item key="listview"><Link to={'/layout/log/special'}><span style={{fontSize:'15px'}}><b>审批列表查看</b></span></Link></Menu.Item>
          </SubMenu>):null
        }
        </Menu>
      </Header>
    )
  }
}

export default MainHeader;
