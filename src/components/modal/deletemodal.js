import React from 'react';
import { Modal, Button} from 'antd';
import 'antd/dist/antd.css';
import axios from 'axios';
import Swal from 'sweetalert';
import PropTypes from "prop-types";

class  Deletemodal extends React.Component{
  static contextTypes = {
    router: PropTypes.object
  }
  constructor(props,context){
    super(props,context);
    this.state={
      visible:false
    }
    this.showModal = this.showModal.bind(this);
    this.handleOk = this.handleOk.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  showModal(){
    this.setState({
      visible:true,
    });
  }

  handleOk(){
    //发送ajax请求,删除某条数据
    const refreshData = this.props.refreshData;
    var username = this.props.username;
    let history = this.context.router.history;
    let self = this;
    axios({
      method:"DELETE",
      url:'http://localhost:8901/app/data',
      datatype:'json',
      data:{key:this.props.record.key,dataname:this.props.dataname}
        }).then(function(result) {
      Swal("删除完成！").then(function(){
          if(self.props.record.account === username){
            sessionStorage.removeItem('userinfo');
            history.push('/login');
          }else{
            refreshData();
          }
          let datatype={users:'用户',tasks:'案件',aims:'目标'};
          let dataname = self.props.dataname;
          let dataaims={users:self.props.record.account,tasks:self.props.record.task_name,aims:self.props.record.aim_name};
          axios({
            method:"POST",
            url:'http://localhost:8901/app/logstash',
            datatype:'json',
            data:{data:{user:username,action:'删除',time:new Date().getTime(),desp:'删除'+datatype[dataname]+dataaims[dataname]}}
              }).then(function(result) {
            console.log("插入日志");
          }).catch(function(err){
               console.log(err);
             });
            });
       }).catch(function(err){
         console.log(err);
       });
    this.setState({
      visible:false,
    });
  }
  handleCancel(){
    this.setState({
      visible:false,
    });
  }
  render(){
    return(
      <span>
      <Button type="primary" onClick={this.showModal}>{this.props.name}</Button>
      <Modal
      title={this.props.name}
      visible={this.state.visible}
      onOk={this.handleOk}
      onCancel={this.handleCancel}
      okText="确认"
      cancelText="取消"
      >
      <div>
      你确定要删除该条数据吗？
      </div>
      </Modal>
      </span>
    );
  }
}

export default Deletemodal;
