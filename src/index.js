import React from 'react';
import ReactDOM from 'react-dom';
import { LocaleProvider } from 'antd';
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import 'moment/locale/zh-cn';
import './index.css';
import RouteFile from './router';
import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import Reducers from './reducer';

const store = createStore(Reducers, compose(
    applyMiddleware(thunk)
))

ReactDOM.render(
      <Provider store={store}>
        <LocaleProvider locale={zh_CN}>
          <RouteFile />
        </LocaleProvider>
      </Provider>,
    document.getElementById('root'));
