var express = require('express');
var app = express();
var admin = express();
var _ = require('lodash');
var bodyParser = require('body-parser');
var userRegister = require('./esuserregister');
var loginVerify = require('./esuserverify');
var getFields = require('./esgetfields');
var getData = require('./esgetdata');
var postData = require('./espostdata');
var deleteData = require('./esdeletedata');
var updateData = require('./esupdatedata');
var getUsers = require('./esgetusers');
var getContents = require('./esgetcontents');
var changePass = require('./eschangepass');
var getStatic = require('./esgetstatic');
var getAimsCount = require('./esgetaimscount');
var getAims = require('./esgetaims');
var getTaskName = require('./esgettaskname');
var getTask = require('./esgettask');
var recentView = require('./esrecentview');
var postRecentView = require('./espostview');
var logstash = require('./espostlogs');

app.use(bodyParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With,Content-Type");
    res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
    res.header("Access-Control-Max-Age",0);
    next();
});
app.listen(8901);

admin.post('/register', function(req, res) {
    userRegister(req.body,function(data){
      res.send(data);
    });
});

admin.post('/login', function(req, res) {
    loginVerify(req.body,function(data){
      res.send(data);
    });
});

admin.get('/fields', function(req, res) {
  getFields(req.query.id,function(data){
    res.send(data);
  });
});

admin.get('/data', function(req, res) {
  getData(req.query,function(data){
    res.send(data);
    });
});

admin.post('/data',function(req,res){
    postData(req.body,function(data){
      res.send(data);
  });
});

admin.delete('/data',function(req,res){
    deleteData(req.body,function(data){
      res.send(data);
  });
});

admin.post('/update',function(req,res){
    updateData(req.body,function(data){
      res.send(data);
  });
});

admin.get('/users',function(req,res){
    getUsers(req.query,function(data){
      res.send(data);
    });
});
admin.get('/contentfors',function(req,res){
    getContents(req.query,function(data){
      res.send(data);
    })
})

admin.post('/changepass',function(req,res){
    changePass(req.body,function(data){
      res.send(data);
    })
})

admin.get('/static',function(req,res){
   getStatic(req.query,function(data){
     res.send(data);
   })
});

admin.get('/aimscount',function(req,res){
   getAimsCount(req.query,function(data){
     res.send(data);
   })
});

admin.get('/aims',function(req,res){
   getAims(req.query,function(data){
     res.send(data);
   })
});

admin.get('/taskname',function(req,res){
   getTaskName(req.query,function(data){
     res.send(data);
   })
});

admin.get('/taskmessage',function(req,res){
   getTask(req.query,function(data){
     res.send(data);
   })
});

admin.get('/recentview',function(req,res){
   recentView(req.query,function(data){
     res.send(data);
   })
});

admin.post('/recentview',function(req,res){
   postRecentView(req.body,function(data){
     res.send(data);
   })
});

admin.post('/logstash',function(req,res){
   logstash(req.body,function(data){
     res.send(data);
   })
});
app.use('/app',admin);
