//新建索引为每个索引的数据以及对应索引的中英文字段名对照
//初步确定es索引数据为indexname/wbwj/randomnum;索引中英文字段对照索引命名规范为wbwj/fields/fieldsname
//用户管理信息、审批任务列表信息、审批目标列表信息、日志信息

//数据格式如下：
const data = [{
  key:1,
  name:"John Brown",
  age: 42,
  address: "New York No.1 Lake Park go strate this way and drop down xxx",
  interest: "New York No.1 Lake Park go strate this way and drop down xxx",
  balance: "New York No.1 Lake Park go strate this way and drop down xxx"
},{
  key:2,
  name:"Snow",
  age: 32,
  address: "New York No.1 Lake Park"
},{
  key:3,
  name:"Air",
  age: 36,
  address: "New York No.1 Lake Park"
},{
  key:4,
  name:"Sun",
  age: 52,
  address: "New York No.1 Lake Park"
},{
  key:5,
  name:"John Brown",
  age: 42,
  address: "New York No.1 Lake Park"
},{
  key:6,
  name:"Snow",
  age: 32,
  address: "New York No.1 Lake Park"
},{
  key:7,
  name:"Air",
  age: 36,
  address: "New York No.1 Lake Park"
},{
  key:8,
  name:"Sun",
  age: 52,
  address: "New York No.1 Lake Park"
},{
  key:9,
  name:"John Brown",
  age: 42,
  address: "New York No.1 Lake Park"
},{
  key:10,
  name:"Snow",
  age: 32,
  address: "New York No.1 Lake Park"
},{
  key:11,
  name:"Air",
  age: 36,
  address: "New York No.1 Lake Park"
},{
  key:12,
  name:"Sun",
  age: 52,
  address: "New York No.1 Lake Park"
}];

const columns=[{
  key:"name",
  cn:"姓名",
  type:"string",
  width:"18%"
},{
  key:"age",
  cn:"年龄",
  type:"num",
  width:"12%"
},{
  "key":"address",
  cn:"地址",
  type:"string",
  width:"18%"
},{
  key:"interest",
  cn:"兴趣",
  type:"other",
  width:"12%"
},{
  key:"banlance",
  cn:"收入",
  type:"other",
  width:"12%"
},{
  key:"action",
  cn:"操作",
  type:"action"}];

  //用户信息包含如下列：
  //账号，姓名，系统角色，密码，部门，状态

  //任务审批信息单包含如下字段：
  //审批单号，任务名称，参与人，负责人，审批人，任务描述，任务状态，审批状态，提交时间

  //目标审批信息单包含如下字段：
  //审批单号，提交时间，名称，所属任务，任务类型，目标，资源范围，结果可见域，描述，目标状态，审批状态

  //日志信息包括：
  //用户名，角色，操作，时间，描述

  curl -XPUT 'http://localhost：9200/wbwj/fields/users' -d' {
  columns:[{
      key:"account",
      cn:"账号",
      type:"string",
      width:"12%"
    },{
      key:"name",
      cn:"姓名",
      type:"string",
      width:"12%"
    },{
      "key":"roles",
      cn:"系统角色",
      type:"string",
      width:"18%"
    },{
      key:"password",
      cn:"密码",
      type:"other",
      width:"12%"
    },{
      key:"apartment",
      cn:"部门",
      type:"other",
      width:"12%"
    },{
      key:"state",
      cn:"状态",
      type:"num"}]
  }'
