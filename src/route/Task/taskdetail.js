import React from 'react';
import axios from 'axios';
import {Divider,Table,Layout} from 'antd';
import MainPage from './../../components/content/mainpage';
import CommonTables from './../../components/tables';
import Addaimsmodal from './../../components/modal/addaimsmodal';
import Commonmodal from './../../components/modal/commonmodal';
import Modifymodal from './../../components/modal/modifymodal';
import Deletemodal from './../../components/modal/deletemodal';
import Viewmodal from './../../components/modal/viewmodal';
import {Link} from 'react-router-dom';

class ModalMesg extends React.Component{
  render(){
    if(this.props.record.aim_approve_state===0){
      return( <div>
        <Commonmodal
        key = "common"
        name = "编辑"
        username = {this.props.username}
        record = {this.props.record}
        display={this.props.display}
        refreshData = {this.props.refreshData}
        dataname = "aims"/ >
        <Divider
        key = "divider"
        type = "vertical" / >
        <Deletemodal
        key = "delete"
        name = "删除"
        username = {this.props.username}
        record = {this.props.record}
        refreshData = {this.props.refreshData}
        dataname = "aims"/ >
        </div>)
    }else{
      return(
        <div>
          <Viewmodal
          key = "view"
          name = "查看"
          username = {this.props.username}
          record = {this.props.record}
          display={this.props.display}
          refreshData = {this.props.refreshData}
          dataname = "tasks"/ >
        </div>
      );
    }
  }
}

class TaskDetail extends React.Component{
  constructor(props){
    super(props);
    this.state={
      username:null,
      task_data:[],
      task_columns:[],
      aim_data:[],
      aim_columns:[],
      taskpathname:null
    }
    this.refreshData = this.refreshData.bind(this);
  }
  componentWillMount() {
    var userinfo = sessionStorage.getItem('userinfo');
    let self=this;
    var taskmessage = JSON.parse(sessionStorage.getItem('taskmessage'));
    var taskpath = sessionStorage.getItem('taskpath').split('/');
    this.setState({
      username:JSON.parse(userinfo).account,
      task_data:[taskmessage],
      taskpathname:taskpath[taskpath.length-1]
    })
    axios({
     method:"GET",
     url:'http://localhost:8901/app/fields?id=finish_tasks',
     data:[]
       }).then(function(result) {
         self.setState({
           task_columns:result.data.columns
               });
      }).catch(function(err){
        console.log(err);
      });
    axios({
      method:"GET",
      url:'http://localhost:8901/app/static?taskname='+taskmessage.task_name,
      data:[]
        }).then(function(result) {
          let aimsdata=result.data.map((item)=>
         item._source);
     self.setState({
      aim_data:aimsdata
          });
       }).catch(function(err){
         console.log(err);
       });
   axios({
     method:"GET",
     url:'http://localhost:8901/app/fields?id=finish_aims',
     data:[]
       }).then(function(result) {
         self.setState({
           aim_columns:result.data.columns
               });
      }).catch(function(err){
        console.log(err);
      });
  }
  refreshData(){
    let self=this;
    let taskmessage = JSON.parse(sessionStorage.getItem('taskmessage'));
    axios({
      method:"GET",
      url:'http://localhost:8901/app/taskmessage?key='+taskmessage.key,
        }).then(function(result) {
        let tasksdata=result.data.map((item)=>
          item._source);
        self.setState({
         task_data:tasksdata
             });
       }).catch(function(err){
         console.log(err);
       });
   axios({
     method:"GET",
     url:'http://localhost:8901/app/static?taskname='+taskmessage.task_name,
     data:[]
       }).then(function(result) {
         let aimsdata=result.data.map((item)=>
        item._source);
    self.setState({
     aim_data:aimsdata
         });
      }).catch(function(err){
        console.log(err);
      });

  }
  render(){
    function Displaytime(columns){
         if(columns.key!=='reservation'){
             return (text,record)=>(
                  <span>
                  {new Date(text).toLocaleString()}
                  </span>
              );
           }else{
             return (text,record)=>(
                  <span>
                  {new Date(text[0]).toLocaleDateString()+'~'+new Date(text[1]).toLocaleDateString()}
                  </span>
              );
           }
         }
    function DispTaskStatus(columns){
     let task_state = {"0":"启动","1":"归档"};
     let task_approve_state = {"0":"待审批","1":"通过","2":"拒绝"};

        switch (columns.key) {
          case 'task_state':
           return (text,record)=>( <span>{task_state[text]} </span>);
          //  break;
          case 'task_approve_state':
           return (text,record)=>(<span>{task_approve_state[text]} </span>);
          // break;
          default:
           return (text,record)=>(<span>{text}</span>);
        }
      }
    function DispAimStatus(columns){
      let task_state = {"0":"启动","1":"归档"};
      let aim_approve_state = {"0":"待审批","1":"通过","2":"拒绝"};
      let aim_option = {"0":"核查","1":"中标","2":"资源申请"};
      let aim_view = {"0":"只显号码","1":"只显内容","2":"全部显示"};

      switch (columns.key) {
      case 'aim_state':
       return (text,record)=>( <span>{task_state[text]} </span>);
      //  break;
      case 'aim_approve_state':
       return (text,record)=>(<span>{aim_approve_state[text]} </span>);
      // break;
     case 'aim_option':
      return (text,record)=>( <span>{aim_option[text]} </span>);
      // break;
     case 'aim_view':
      return (text,record)=>(<span>{aim_view[text]} </span>);
      // break;
      default:
       return (text,record)=>(<span>{text}</span>);
           }
         }
    function DispTaskOther(columns){
       switch (columns.key) {
         case 'task_member':
          return (text,record)=>( <span>{text.join(' | ')} </span>);
        //   break;
         default:
          return (text,record)=>(<span>{text}</span>);
       }
     }
    let task_columns = this.state.task_columns.slice();
    let aim_columns = this.state.aim_columns.slice();
    let username = this.state.username;
    for(var i in task_columns){
      if(task_columns[i].type==="action"){
        task_columns[i]={
          title:task_columns[i].cn,
          key:task_columns[i].key,
          dataIndex:task_columns[i].key,
          display:task_columns[i].display,
          render: (text, record) => ( <div>
            <Modifymodal
            key = "modify"
            name = "编辑"
            username = {username}
            record = {record}
            display={task_columns}
            refreshData = {this.refreshData}
            dataname = "tasks"/ >
            <Divider
              key = "divider"
              type = "vertical" / >
            <Addaimsmodal
            key = "addaims"
            name = "新增目标"
            username = {username}
            record = {record}
            display={aim_columns}
            refreshData = {this.refreshData}
            dataname = "aims"/ >
              </div>)
        };
      }else if(task_columns[i].type==="time"){
        task_columns[i]={title:task_columns[i].cn,
          key:task_columns[i].key,
          dataIndex:task_columns[i].key,
          display:task_columns[i].display,
          render:Displaytime(task_columns[i])
        };
      }else if(task_columns[i].type==="num"){
        task_columns[i]={
          title:task_columns[i].cn,
          key:task_columns[i].key,
          dataIndex:task_columns[i].key,
          display:task_columns[i].display,
          render:DispTaskStatus(task_columns[i])
        };
      }else if(task_columns[i].type==="other"){
        task_columns[i]={
          title:task_columns[i].cn,
          key:task_columns[i].key,
          dataIndex:task_columns[i].key,
          display:task_columns[i].display,
          render:DispTaskOther(task_columns[i])
        };
      }else{
        task_columns[i]={
          title:task_columns[i].cn,
          key:task_columns[i].key,
          dataIndex:task_columns[i].key,
          display:task_columns[i].display,
          width:task_columns[i].width
        };
      }
    }
    for(var j in aim_columns){
      if(aim_columns[j].type==="action"){
        aim_columns[j]={title:aim_columns[j].cn,
          key:aim_columns[j].key,
          dataIndex:aim_columns[j].key,
          display:aim_columns[j].display,
          render: (text, record) => (
            <ModalMesg
            username = {username}
            record = {record}
            display={aim_columns}
            refreshData = {this.refreshData}
            />
          )
        };
      }else if(aim_columns[j].type==="time"){
        aim_columns[j]={
          title:aim_columns[j].cn,
          key:aim_columns[j].key,
          dataIndex:aim_columns[j].key,
          display:aim_columns[j].display,
          render:Displaytime(aim_columns[j])};
      }else if(aim_columns[j].type==="num"){
        aim_columns[j]={
          title:aim_columns[j].cn,
          key:aim_columns[j].key,
          dataIndex:aim_columns[j].key,
          display:aim_columns[j].display,
          render:DispAimStatus(aim_columns[j])};
      }else{
        aim_columns[j]={
          title:aim_columns[j].cn,
          key:aim_columns[j].key,
          dataIndex:aim_columns[j].key,
          display:aim_columns[j].display,
          width:aim_columns[j].width
        };
      }
    }
    let tasklink={"taskmanage":<Link to={'/layout/task/taskmanage'}>已审批案件</Link>,"taskfinish":<Link to={'/layout/task/taskfinish'}>已审批案件</Link>}
    return(
      <Layout>
      <MainPage crumb={[<Link to={'/layout/task/statistic'}>案件管理</Link>,tasklink[this.state.taskpathname],"案件管理"]}>
      <h2>案件详情</h2>
      <Table
      columns={task_columns}
      dataSource={this.state.task_data}
      />
      <br />
      <CommonTables
      title="目标列表"
      columns={aim_columns}
      data={this.state.aim_data}
      tabletitle={() =><div></div>}
      />
      </MainPage>
      </Layout>
    );
  }
}

export default TaskDetail;
