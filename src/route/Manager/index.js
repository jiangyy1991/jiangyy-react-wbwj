import React from 'react';
import { Route ,Switch} from 'react-router-dom';
import UserManage from './usermanage';

class  ManagerRoute extends React.Component{
  render(){
    return(
      <Switch>
      <Route exact path="/layout/manage/usermanage" component={UserManage} />
      </Switch>
    )
  }
}

export default ManagerRoute;
