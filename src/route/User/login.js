import React from 'react';
import {Link} from 'react-router-dom';
import '../../css/assets/css/amazeui.min.css'
import '../../css/assets/css/amazeui.datatables.min.css'
import '../../css/assets/css/app.css'
import './login.css'
import axios from 'axios';
import Swal from 'sweetalert';
import imgURL from './picture/logo_login.png';
import {connect} from 'react-redux';
import {PutAccount} from './../../reducer/userinfo';


class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            userpass: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit(event) {
      let self = this;
      event.preventDefault();
      var pages = {"0":'/task/statistic',"1":'/approve/statistic',"2":'/manage/usermanage',"3":'/log/statistic'}
        axios({
            method: "POST",
            url: 'http://localhost:8901/app/login',
            data: {username:this.state.username,password:this.state.userpass}
        }).then(function (result) {
            sessionStorage.setItem('userinfo', JSON.stringify(result.data._source));
            var userinfo = sessionStorage.getItem('userinfo');
            if (userinfo && userinfo !== null && userinfo !== 'undefined') {
              self.props.dispatch(PutAccount(JSON.parse(userinfo).account));
              var userrole = JSON.parse(userinfo).roles.sort();
              var loginpage = pages[userrole[0]];
                self.props.history.push('/layout'+loginpage);
            }else{
              Swal({
                text: "用户名或密码错误！",
                buttons: {
                  cancel: false,
                  confirm: "确定"
                  }
              });
            }
        }).catch(function (err) {
            console.log(err);
        });
    }

    render() {
        return (
            <div className="am-g tpl-g">
                <div className="tpl-login">
                    <div className = "tpl-login-content">
                        <div>
                            <img src={imgURL} alt="文本数据挖掘平台的图标" className="picture-style"/>
                            <div className="title-style">文本数据挖掘平台</div>
                        </div>
                        <form onSubmit={(event)=>this.handleSubmit(event)} className="am-form tpl-form-line-form">
                            <div className="am-form-group">
                                <input type="text" name="username" value={this.state.username} placeholder="请输入帐号" autoComplete="OFF" onChange={this.handleChange}/>
                            </div>
                            <div className="am-form-group">
                                <input type="password" name="userpass" value={this.state.userpass} placeholder="请输入密码" onChange={this.handleChange} />
                            </div>
                            <div className="am-form-group">
                                <button className="am-btn am-btn-primary  am-btn-block tpl-btn-bg-color-success  tpl-login-btn">登录</button>
                            </div>
                            <div className="am-form-group">
                                <Link className="linktoregister" to="/register">跳转到注册页面</Link>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

// export default connect(
//   state=>state.UserInfo
// )(Login);

export default connect()(Login);
