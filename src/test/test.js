import React from 'react';

import { Collapse } from 'antd';
const Panel = Collapse.Panel;

let data={items:[
  {name:'panel1',pans:[{name:'123',pans:[{name:'3212',pans:[{name:'992',href:'3213'},{name:'1424',pans:[{name:'1212',pans:[{name:'123',href:"dsda"}]}]}]},{name:'483',href:'www.34.com'}]},{name:'43',href:'www.34.com'}]},
  {name:'panel2',pans:[{name:'321',href:'www.123.com'},{name:'123',href:'www.321.com'}]},
  {name:'panel3',pans:[{name:'543',href:'www.234.com'},{name:'123',href:'www.321.com'}]},
  {name:'43',href:'www.34.com'}
]}

class Items extends React.Component{
  constructor(props){
    super(props);
  }
  render(){
    let pan = this.props.data;
    if(this.props.data.pans){
      return(
      <div><Collapse><Panel key={pan.name} header={pan.name}><Pans data={pan.pans}/></Panel></Collapse></div>
      )
    }else{
      return(
      <div key={pan.name}>{pan.name}</div>
    )
    }

  }
}
class Pans extends React.Component{
  constructor(props) {
    super(props);
  }
  render(){
    return(
      <div>
      {this.props.data.map(pan=>
        <Items key={pan.name} data={pan}/>
              )}
      </div>
    )
  }
}
class Test extends React.Component{
  constructor(props){
    super(props);
  }
  render(){
    function callback(key) {
      console.log(key);
    }
    return(
      <div>
      {
        data.items.map(item=>
        <Items key={item.name} data={item}/>)
      }
    </div>
    );
  }
}

export default Test;
