const elasticsearch = require('elasticsearch');

module.exports = function(querystr,callback){
  const esClient_post = new elasticsearch.Client({
    host:'localhost:9200',
    log:'error'
});

var querybody;
if(querystr.roles.indexOf(',')){
  querybody={query:{terms:{roles:querystr.roles.split(',')}}};
}else{
  querybody={query:{term:{roles:querystr.roles}}};
}
  esClient_post.search({
    index:"users",
    type:"wbwj",
    body:querybody,
    _source:"account",
  }).then(function(re){
    callback(re.hits.hits);
  },function(err){
    console.trace(err.message);
  });

}
