import React from 'react';
import FormList from './../form/formlist';
import {Form, Modal, Button} from 'antd';
import 'antd/dist/antd.css';
import axios from 'axios';
import Swal from 'sweetalert';
import PropTypes from "prop-types";

class  Commonmodal extends React.Component{
  static contextTypes = {
    router: PropTypes.object
  }
  constructor(props,context){
    super(props,context);
    this.state={
      visible:false
    }
    this.showModal = this.showModal.bind(this);
    this.handleOk = this.handleOk.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  showModal(){
    this.setState({
      visible:true,
    });
  }

  handleOk(){
    const { getFieldsValue, resetFields } = this.props.form;
    let history = this.context.router.history;
    this.props.form.validateFieldsAndScroll((err, values) => {
    if (!err) {
    const values = getFieldsValue();
    if(sessionStorage.getItem('userinfo')!=='undefined' && sessionStorage.getItem('userinfo')!==null){
      var userinfo = JSON.parse(sessionStorage.getItem('userinfo'));
      var username = userinfo.account;
    }
    if(this.props.dataname==='unfinish_tasks'){
      values.task_create=username;
      values.task_edittime=new Date().getTime();
    }else if(this.props.dataname==='unfinish_aims'){
      values.aim_create=username;
      values.aim_edittime=new Date().getTime();
    }
    if(this.props.dataname==='users'&&username===values.account){
      userinfo=values;
      sessionStorage.setItem('userinfo', JSON.stringify(userinfo));
    }
    let dataname = this.props.dataname;
    //发送ajax请求。
    const refreshData = this.props.refreshData;
    axios({
      method:"POST",
      url:'http://localhost:8901/app/update',
      datatype:'json',
      data:{querystr:{key:this.props.record.key},fields:values,dataname:this.props.dataname}
        }).then(function(result) {
          Swal("编辑成功！").then(function(){
            if(username===values.account&&values.roles.indexOf("2")===-1&&dataname==='users'){
              var pages = {"0":'/task',"1":'/approve',"2":'/usermanage',"3":'/log'}
              var userrole = values.roles.sort();
              history.push(pages[userrole[0]]);
            }else {
              refreshData();
                 }
         let datatype = {tasks:'案件',aims:'目标',users:'用户'}
         let dataaims = {tasks:values.task_name,aims:values.aim_name,users:values.account}
         axios({
           method:"POST",
           url:'http://localhost:8901/app/logstash',
           datatype:'json',
           data:{data:{user:username,action:'编辑',time:new Date().getTime(),desp:'编辑'+datatype[dataname]+dataaims[dataname]}}
             }).then(function(result) {
           console.log("插入日志");
         }).catch(function(err){
              console.log(err);
            });

              }
            )
       }).catch(function(err){
         console.log(err);
       });
    //重置表单 (坑点)
    resetFields();
    this.setState({
      visible:false,
    });
  }
});
  }
  handleCancel(){
    const {resetFields}= this.props.form;
    resetFields();
    this.setState({
      visible:false,
    });
  }

  render(){
    let displays = this.props.display.slice();
    let disabled=[];
    for(var i in displays){
      if(displays[i].display!=='none'){
        disabled[displays[i].key]=false;
      }
    }
    return(
      <span>
      <Button type="primary" onClick={this.showModal}>{this.props.name}</Button>
      <Modal
      title={this.props.name}
      visible={this.state.visible}
      onOk={this.handleOk}
      onCancel={this.handleCancel}
      okText="确认"
      cancelText="取消"
      >
      <FormList form={this.props.form} disabled={disabled} display={this.props.display} record={this.props.record}/>
      </Modal>
      </span>
    );
  }
}

export default Form.create()(Commonmodal);
