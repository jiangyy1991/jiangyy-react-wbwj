import React from 'react';
import axios from 'axios';
import {Layout} from 'antd';
import MainPage from './../../../components/content/mainpage';
import CommonTables from './../../../components/tables';
import Viewmodal from './../../../components/modal/viewmodal';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';

class AimFinish extends React.Component{
  constructor(props){
    super(props);
    this.state={
      finish_data:[],
      finish_columns:[],
      username:null
    }
    this.refreshData = this.refreshData.bind(this);
  }
  componentWillMount() {
    var userinfo = sessionStorage.getItem('userinfo');
      if (this.props.UserInfo.account===null) {
        this.setState({
          username: JSON.parse(userinfo).account
        });
      }else{
      this.setState({
        username: this.props.account
      });
    }
    }
    componentDidMount(){
    let self = this;
    if(this.props.AimsFinish.finish_aims!==null){
      this.setState({
        finish_data:this.props.AimsFinish.finish_aims.map((item)=>
       item._source)
      })
    }else{
    axios({
      method:"GET",
      url:'http://localhost:8901/app/data?type=users&&data=finish_aims&&username='+this.state.username,
      data:[]
        }).then(function(result) {
          let aimsdata=result.data.map((item)=>
         item._source);
     self.setState({
      finish_data:aimsdata
          });
       }).catch(function(err){
         console.log(err);
       });
    }
    axios({
     method:"GET",
     url:'http://localhost:8901/app/fields?id=finish_aims',
     data:[]
       }).then(function(result) {
         self.setState({
           finish_columns:result.data.columns
               });
      }).catch(function(err){
        console.log(err);
      });
}
  refreshData(){
    let self=this;
    let username = this.state.username;
    axios({
      method:"GET",
      url:'http://localhost:8901/app/data?type=users&&data=finish_aims&&username='+username,
        }).then(function(result) {
        let tasksdata=result.data.map((item)=>
          item._source);
      self.setState({
       finish_data:tasksdata
           });
       }).catch(function(err){
         console.log(err);
       });

  }
  render(){
    function Displaytime(columns){
         if(columns.key!=='reservation'){
             return (text,record)=>(
                  <span>
                  {new Date(text).toLocaleString()}
                  </span>
              );
           }else{
             return (text,record)=>(
                  <span>
                  {new Date(text[0]).toLocaleDateString()+'~'+new Date(text[1]).toLocaleDateString()}
                  </span>
              );
           }
         }
    function DispAimStatus(columns){
     let aim_state = {"0":"启动","1":"归档"};
     let aim_approve_state = {"0":"待审批","1":"通过","2":"拒绝"};
     let aim_option = {"0":"核查","1":"中标","2":"资源申请"};
     let aim_view = {"0":"只显号码","1":"只显内容","2":"全部显示"};

     switch (columns.key) {
     case 'aim_state':
      return (text,record)=>( <span>{aim_state[text]} </span>);
    //   break;
     case 'aim_approve_state':
      return (text,record)=>(<span>{aim_approve_state[text]} </span>);
    //  break;
    case 'aim_option':
     return (text,record)=>( <span>{aim_option[text]} </span>);
    //  break;
    case 'aim_view':
     return (text,record)=>(<span>{aim_view[text]} </span>);
    // break;
     default:
      return (text,record)=>(<span>{text}</span>);
          }
        }
    let finish_columns = this.state.finish_columns.slice();
    let username = this.state.username;
    for(var i in finish_columns){
      if(finish_columns[i].type==="action"){
        finish_columns[i]={
          title:finish_columns[i].cn,
          key:finish_columns[i].key,
          dataIndex:finish_columns[i].key,
          display:finish_columns[i].display,
          render: (text, record) => ( <div>
              <Viewmodal
              key = "view"
              name = "查看"
              username = {username}
              display = {finish_columns}
              record = {record}
              dataname = "finish_aims" / >
              </div>)
        };
      }else if(finish_columns[i].type === "time") {
        finish_columns[i] = {
          title: finish_columns[i].cn,
          key: finish_columns[i].key,
          dataIndex: finish_columns[i].key,
          display: finish_columns[i].display,
          render: Displaytime(finish_columns[i])
        };
      }else if(finish_columns[i].type === "num") {
        finish_columns[i] = {
          title: finish_columns[i].cn,
          key: finish_columns[i].key,
          dataIndex: finish_columns[i].key,
          display: finish_columns[i].display,
          render: DispAimStatus(finish_columns[i])
        };
      }else{
        finish_columns[i]={
          title:finish_columns[i].cn,
          key:finish_columns[i].key,
          dataIndex:finish_columns[i].key,
          display:finish_columns[i].display,
          width:finish_columns[i].width
        };
      }
    }

    return(
      <Layout>
      <MainPage crumb={[<Link to={'/layout/task/statistic'}>案件管理</Link>,"已审批目标"]}>
      <CommonTables
      title="已审批目标"
      columns={finish_columns}
      data={this.state.finish_data}
      tabletitle={() =><div></div>}
      />
      </MainPage>
      </Layout>
    );
  }
}

export default connect(
  state=>({UserInfo:state.UserInfo,AimsFinish:state.AimsFinish})
)(AimFinish);
