const elasticsearch = require('elasticsearch');

module.exports = function(querystr,callback){
  let username = querystr.username;
  let querybody;
  const esClient_post = new elasticsearch.Client({
  host:'localhost:9200',
  log:'error'
});
  if(querystr.type==='all'){
    //获取案件或目标数据，用于获取全数据（如审批列表查看页面），用户管理页面获取全部用户数据
    querybody={query:{match_all:{}}};
    esClient_post.search({
      index:querystr.data,
      body:querybody,
      size:1000,
    }).then(function(re){
      callback(re.hits.hits);
    },function(err){
      console.trace(err.message);
    });
  }else if(querystr.type==="users"){
//业务员页面获取未审批案件或目标信息
    if(querystr.data.split('_')[1]==='tasks'){
    switch(querystr.data){
      case "unfinish_tasks":
      querybody={query:{bool:{must:[{match_phrase:{task_approve_state:0}},{query_string:{query:username,fields:["task_resp","task_member","task_create"]}}]}}};break;
      case "finish_tasks":
      querybody={query:{bool:{must_not:[{match_phrase:{task_approve_state:0}},{match_phrase:{task_state:1}}],must:[{query_string:{query:username,fields:["task_resp","task_member","task_create"]}}]}}};break;
      case "archived_tasks":
      querybody={query:{bool:{must:[{match_phrase:{task_state:1}},{query_string:{query:username,fields:["task_resp","task_member","task_create"]}}]}}};break;
      default:
      querybody={query:{match_all:{}}};
    }
    esClient_post.search({
      index:"tasks",
      body:querybody,
      size:1000,
    }).then(function(re){
      callback(re.hits.hits);
    },function(err){
      console.trace(err.message);
    });
  }else{
    esClient_post.search({
      index:"tasks",
      type:"wbwj",
      body:{
      query:{
        bool:{
          must:[{
            query_string:{
              query:querystr.username,
              fields:["task_resp","task_member"]
            }},
          {
            match_phrase:{
            task_state:0
          }},
          {
            match_phrase:{
            task_approve_state:1
          }}]}
      }},
      _source:"task_name",
    }).then(function(re){
      let tasks = re.hits.hits.map((item)=>
        item._source.task_name);
      switch(querystr.data){
        case "unfinish_aims":
        querybody={query:{bool:{must:[{match_phrase:{aim_approve_state:0}},{terms:{aim_for:tasks}}]}}};break;
        case "finish_aims":
        querybody={query:{bool:{must_not:[{match_phrase:{aim_approve_state:0}}],must:[{terms:{aim_for:tasks}}]}}};break;
        default:
        querybody={query:{match_all:{}}};
      }
      esClient_post.search({
        index:'aims',
        body:querybody,
        size:1000,
      }).then(function(re){
        console.log(re.hits.hits);
        callback(re.hits.hits);
      },function(err){
        console.trace(err.message);
      });
    },function(err){
      console.trace(err.message);
    });
  }
  }else if(querystr.type==="approve"){
    //审批员页面获取案件或目标数据
    if(querystr.data.split('_')[1]==='tasks'){
    switch(querystr.data){
      case "unfinish_tasks":
      querybody={query:{bool:{must:[{match_phrase:{task_approve_state:0}},{query_string:{query:username,fields:["task_approver"]}}]}}};break;
      case "finish_tasks":
      querybody={query:{bool:{must_not:[{match_phrase:{task_approve_state:0}},{match_phrase:{task_approve_state:3}}],must:[{query_string:{query:username,fields:["task_approver"]}}]}}};break;
      default:
      querybody={query:{match_all:{}}};
    }
    esClient_post.search({
      index:"tasks",
      body:querybody,
      size:1000,
    }).then(function(re){
      callback(re.hits.hits);
    },function(err){
      console.trace(err.message);
    });
  }else{
    esClient_post.search({
      index:"tasks",
      type:"wbwj",
      body:{
      query:{
        bool:{
          must:[{
            query_string:{
              query:querystr.username,
              fields:["task_approver"]
            }},
          {
            match_phrase:{
            task_state:0
          }},
          {
            match_phrase:{
            task_approve_state:1
          }}]}
      }},
      _source:"task_name",
    }).then(function(re){
      let tasks = re.hits.hits.map((item)=>
        item._source.task_name);
      switch(querystr.data){
        case "unfinish_aims":
        querybody={query:{bool:{must:[{match_phrase:{aim_approve_state:0}},{terms:{aim_for:tasks}}]}}};break;
        case "finish_aims":
        querybody={query:{bool:{must_not:[{match_phrase:{aim_approve_state:0}}],
          must:[{terms:{aim_for:tasks}}]}}};break;
            default:
            querybody={query:{match_all:{}}};
      }
      esClient_post.search({
        index:'aims',
        body:querybody,
        size:1000,
      }).then(function(re){
        callback(re.hits.hits);
      },function(err){
        console.trace(err.message);
      });
    },function(err){
      console.trace(err.message);
    });
    }
  }
}
