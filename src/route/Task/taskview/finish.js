import React from 'react';
import axios from 'axios';
import {Divider,Layout} from 'antd';
import MainPage from './../../../components/content/mainpage';
import CommonTables from './../../../components/tables';
import Archivemodal from './../../../components/modal/archivemodal';
import Detail from './../detail/detail';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';

class TaskFinish extends React.Component{
  constructor(props){
    super(props);
    this.state={
      finish_data:[],
      finish_columns:[],
      username:null
    }
    this.refreshData = this.refreshData.bind(this);
  }
  componentWillMount() {
    var userinfo = sessionStorage.getItem('userinfo');
      if (this.props.UserInfo.account===null) {
        this.setState({
          username: JSON.parse(userinfo).account
        });
      }else{
      this.setState({
        username: this.props.account
      });
    }
    }
    componentDidMount(){
    let self = this;
    if(this.props.TaskFinish.finish_tasks!==null){
      this.setState({
        finish_data:this.props.TaskFinish.finish_tasks.map((item)=>
       item._source)
      })
    }else{
    axios({
      method:"GET",
      url:'http://localhost:8901/app/data?type=users&&data=finish_tasks&&username='+this.state.username,
      data:[]
        }).then(function(result) {
          let tasksdata=result.data.map((item)=>
         item._source);
     self.setState({
      finish_data:tasksdata
          });
       }).catch(function(err){
         console.log(err);
       });
    }
    axios({
     method:"GET",
     url:'http://localhost:8901/app/fields?id=finish_tasks',
     data:[]
       }).then(function(result) {
         self.setState({
           finish_columns:result.data.columns
               });
      }).catch(function(err){
        console.log(err);
      });
}
  refreshData(){
   let self=this;
   let username = this.state.username;
   axios({
     method:"GET",
     url:'http://localhost:8901/app/data?type=users&&data=finish_tasks&&username='+username,
       }).then(function(result) {
       let tasksdata=result.data.map((item)=>
         item._source);
       self.setState({
        finish_data:tasksdata
            });
      }).catch(function(err){
        console.log(err);
      });

  }
  render(){
    function Displaytime(columns){
         if(columns.key!=='reservation'){
             return (text,record)=>(
                  <span>
                  {new Date(text).toLocaleString()}
                  </span>
              );
           }else{
             return (text,record)=>(
                  <span>
                  {new Date(text[0]).toLocaleDateString()+'~'+new Date(text[1]).toLocaleDateString()}
                  </span>
              );
           }
         }
    function DispTaskOther(columns){
      switch (columns.key) {
        case 'task_member':
         return (text,record)=>( <span>{text.join(' | ')} </span>);
        //  break;
        default:
         return (text,record)=>(<span>{text}</span>);
      }
    }
    function DispTaskStatus(columns){
     let task_state = {"0":"启动","1":"归档"};
     let task_approve_state = {"0":"待审批","1":"通过","2":"拒绝"};

        switch (columns.key) {
          case 'task_state':
           return (text,record)=>( <span>{task_state[text]} </span>);
          //  break;
          case 'task_approve_state':
           return (text,record)=>(<span>{task_approve_state[text]} </span>);
          // break;
          default:
           return (text,record)=>(<span>{text}</span>);
        }
      }
    let finish_columns = this.state.finish_columns.slice();
    let username = this.state.username;
    for(var i in finish_columns){
      if(finish_columns[i].type==="action"){
        finish_columns[i]={
          title:finish_columns[i].cn,
          key:finish_columns[i].key,
          dataIndex:finish_columns[i].key,
          display:finish_columns[i].display,
          render: (text, record) => ( <div>
              <Detail username={username} data={record} display={finish_columns}/>
              <Divider
              key = "divider"
              type = "vertical" / >
              <Archivemodal
              key = "archive"
              name = "归档"
              username = {username}
              record = {record}
              refreshData = {this.refreshData}
              dataname = "finish_tasks"/ >
              </div>)
        };
      }else if(finish_columns[i].type==='time'){
        finish_columns[i]={
          title:finish_columns[i].cn,
          key:finish_columns[i].key,
          dataIndex:finish_columns[i].key,
          display:finish_columns[i].display,
          width:finish_columns[i].width,
          render:Displaytime(finish_columns[i])
        };
      }else if(finish_columns[i].type==='other'){
        finish_columns[i]={
          title:finish_columns[i].cn,
          key:finish_columns[i].key,
          dataIndex:finish_columns[i].key,
          display:finish_columns[i].display,
          width:finish_columns[i].width,
          render:DispTaskOther(finish_columns[i])
        };
      }else if(finish_columns[i].type==='num'){
        finish_columns[i]={
          title:finish_columns[i].cn,
          key:finish_columns[i].key,
          dataIndex:finish_columns[i].key,
          display:finish_columns[i].display,
          width:finish_columns[i].width,
          render:DispTaskStatus(finish_columns[i])
        };
      }else{
        finish_columns[i]={
          title:finish_columns[i].cn,
          key:finish_columns[i].key,
          dataIndex:finish_columns[i].key,
          display:finish_columns[i].display,
          width:finish_columns[i].width
        };
      }
    }

    return(
      <Layout>
      <MainPage crumb={[<Link to={'/layout/task/statistic'}>案件管理</Link>,"已审批案件"]}>
      <CommonTables
      title="已审批案件"
      columns={finish_columns}
      data={this.state.finish_data}
      tabletitle={() =><div></div>}
      />
      </MainPage>
      </Layout>
    );
  }
}

export default connect(
  state=>({UserInfo:state.UserInfo,TaskFinish:state.TaskFinish})
)(TaskFinish);
