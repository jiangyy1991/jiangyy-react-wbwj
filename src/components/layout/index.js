import React from 'react';
import { Layout} from 'antd';
import { Route ,Switch} from 'react-router-dom';
import 'antd/dist/antd.css';
import './index.css';
import MainHeader from './../headers/mainheader';
import SubHeader from './../headers/subheader';
import NoFound from './../../route/ErrorPage/403';
import AuthTask from './auth/AuthTask';
import AuthApprove from './auth/AuthApprove';
import AuthManager from './auth/AuthManager';
import AuthLog from './auth/AuthLog';
import TaskRoute from './../../route/Task';
import ApproveRoute from './../../route/Approve';
import ManagerRoute from './../../route/Manager';
import LogRoute from './../../route/Log';

class LayoutComponent extends React.Component {

  render(){
    return(
      <Layout>
      <MainHeader/>
      <SubHeader />
      <Layout>
         <Switch>
           <AuthTask  path="/layout/task" component={TaskRoute} />
           <AuthApprove  path="/layout/approve" component={ApproveRoute} />
           <AuthManager  path="/layout/manage" component={ManagerRoute} />
           <AuthLog  path="/layout/log" component={LogRoute} />
           <Route exact path="/layout/403" component={NoFound} />
         </Switch>
        </Layout>
      </Layout>
    )
  }
}

export default LayoutComponent;
