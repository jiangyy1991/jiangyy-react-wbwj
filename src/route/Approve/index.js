import React from 'react';
import { Route ,Switch} from 'react-router-dom';
import AimList from './aimlist';
import PendingList from './pendinglist';
import TaskList from './tasklist';
import ApproveStatistic from './statistic';

class  ApproveRoute extends React.Component{
  render(){
    return(
      <Switch>
      <Route exact path="/layout/approve/statistic" component={ApproveStatistic} />
      <Route exact path="/layout/approve/pendinglist" component={PendingList} />
      <Route exact path="/layout/approve/tasklist" component={TaskList} />
      <Route exact path="/layout/approve/aimlist" component={AimList} />
      </Switch>
    )
  }
}

export default ApproveRoute;
