const elasticsearch = require('elasticsearch');

module.exports = function(querystr,callback){
  const esClient_post = new elasticsearch.Client({
  host:'localhost:9200',
  log:'error'
});

    let aimsquery = {
          aggs: {
            aims_count: {
              terms: {
                field: "aim_for.keyword",
                size: 1000
              }
            }
          },
          query: {
            bool: {
              must: [{
                  match_phrase: {
                    aim_option: 0
                  }
                },
                {
                  match_phrase: {
                    aim_approve_state: 1
                  }
                }
              ]
            }
          }
        }

    esClient_post.search({
      index:'aims',
      type:'wbwj',
      body:aimsquery,
    }).then(function(re){
      callback(re.aggregations.aims_count.buckets);
    },function(err){
      console.trace(err.message);
    });
}
