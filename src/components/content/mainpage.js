import React from 'react';
import { Layout,  Breadcrumb} from 'antd';
const { Content} = Layout;


class MainPage extends React.Component {
  // constructor(props) {
  //   super(props);
  // }
  render(){
    return(
          <Layout style={{ padding: '0 24px 24px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
            {this.props.crumb.map(item=>
                <Breadcrumb.Item key={item}>{item}</Breadcrumb.Item>
            )}
            </Breadcrumb>
            <Content style={{ background: '#fff', padding: 24, margin: 0, minHeight: 800 }}>
            {this.props.children}
            </Content>
          </Layout>

    )
  }
}

export default MainPage;
