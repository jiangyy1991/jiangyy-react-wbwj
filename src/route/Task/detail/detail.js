import React from 'react';
import {Link} from 'react-router-dom';
import {Button} from 'antd';
import Viewmodal from './../../../components/modal/viewmodal';

class Detail extends React.Component{
  constructor(props){
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick(){
    sessionStorage.setItem('taskpath', window.location.href);
    sessionStorage.setItem('taskmessage', JSON.stringify(this.props.data));
  }
  render(){
    if(this.props.data.task_approve_state==='2'){
      return(
     <Viewmodal
     key = "view"
     name = "查看"
     username = {this.props.username}
     display = {this.props.display}
     record = {this.props.data}
     dataname = "finish_tasks"
     />
    )
  }else{
    return(
      <Link to='/layout/task/taskdetail'><Button type="primary" onClick={this.handleClick}>{"管理"}</Button></Link>
    );
  }
  }
}

export default Detail;
