import React from 'react';
import axios from 'axios';
import TaskSider from './sider';
import { Layout,Tabs } from 'antd';
import {connect} from 'react-redux';
import MainSider from './../../components/sider/mainsider';
import MainPage from './../../components/content/mainpage';
import TablesPage from './tasklist/tablepage';
import IframesPage from './tasklist/iframe';
const TabPane = Tabs.TabPane;

class Tasks extends React.Component{
  constructor(props){
    super(props);
    this.newTabIndex = 0;
    const panes = [];
    this.state = {
      pathname:this.props.location.pathname.split('/')[3],
      activeKey: '1',
      iFrameHeight: '0px',
      panes,
      username:null
    }
  }

    onChange = (activeKey) => {
      this.setState({ activeKey });
    }
    onEdit = (targetKey, action) => {
      this[action](targetKey);
    }
    add = (data,name,link) => {
      const panes = this.state.panes;
      const activeKey = `newTab${this.newTabIndex++}`;
      let datasession = data;
      datasession.link = link;
      sessionStorage.setItem('recentview', JSON.stringify(datasession));
      panes.push({ title: data.aim_name+'-'+name, content: 'Content of new Tab', key: activeKey });
      this.setState({ panes, activeKey });
      let username = this.state.username;
      axios({
        method:"POST",
        url:'http://localhost:8901/app/recentview',
        datatype:'json',
        data:{dataname:'recentview',data:{"user":this.state.username,"action":'查看','task':data.aim_for,'tabname':data.aim_name+'-'+name,'aim':data.aim_verify,'timeinterval':data.reservation,link:link,state:0,time:new Date().getTime()}}
      }).then(function(result) {
        axios({
          method:"POST",
          url:'http://localhost:8901/app/logstash',
          datatype:'json',
          data:{data:{user:username,action:'查看',time:new Date().getTime(),desp:'查看任务'+data.aim_for+'下目标'+data.aim_name+'的看板'+name+',查询条件：'+data.aim_verify}}
            }).then(function(result) {
          console.log("插入日志");
        }).catch(function(err){
             console.log(err);
           });
         }).catch(function(err){
           console.log(err);
     });
    }
    remove = (targetKey) => {
      let activeKey = this.state.activeKey;
      let lastIndex;
      this.state.panes.forEach((pane, i) => {
        if (pane.key === targetKey) {
          lastIndex = i - 1;
        }
      });
      const panes = this.state.panes.filter(pane => pane.key !== targetKey);
      if (lastIndex >= 0 && activeKey === targetKey) {
        activeKey = panes[lastIndex].key;
      }else if (lastIndex === -1 && activeKey === targetKey) {
        activeKey = '1';
      }
      this.setState({ panes, activeKey });
    }
    changes = () => {
      this.setState({panes:[],activeKey:'1' });
    }
  componentWillMount() {
    var userinfo = sessionStorage.getItem('userinfo');
      if (this.props.account===null) {
        this.setState({
          username: JSON.parse(userinfo).account
        });
      }else{
      this.setState({
        username: this.props.account
      });
    }
}

  render(){
    let pathname=this.props.location.pathname.split('/');
    //let activeKey = this.state.activeKey;
    return(
      <Layout>
      <MainSider>
      <TaskSider selectkey={pathname[3]+'/'+pathname[4]} openkey={["task","tasks",pathname[3]]} changePanes={this.changes}/>
      </MainSider>
      <MainPage crumb={["案件管理"]}>
      <div>
        <h2><b>{"目标列表--"}{pathname[3]}</b></h2>
        <Tabs
          hideAdd
          onChange={this.onChange}
          activeKey={this.state.activeKey}
          type="editable-card"
          onEdit={this.onEdit}
        >
         <TabPane tab={pathname[4]} key={'1'} closable={false}>
         <TablesPage pathname={pathname} paneskey={pathname[4]} panescont={"主面板"} addData = {this.add}/>
         </TabPane>
          {this.state.panes.map(pane =>
            <TabPane tab={pane.title} key={pane.key} closable={pane.closable}>
            <IframesPage />
            </TabPane>)}
        </Tabs>
        </div>
        </MainPage>
      </Layout>
    );
  }
}

export default connect(
  state=>state.UserInfo
)(Tasks);
