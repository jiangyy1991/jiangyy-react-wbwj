import React from 'react';
import { Route ,Switch} from 'react-router-dom';
import TaskStatistic from './statistic';
import AimManage from './aimmanage';
import TaskManage from './taskmanage';
import ArchivedList from './archivedlist';
import AimFinish from './aimview/finish';
import AimUnfinish from './aimview/unfinish';
import TaskFinish from './taskview/finish';
import TaskUnfinish from './taskview/unfinish';
import TaskDetail from './taskdetail';
import Tasks from './tasks';
import RecentView from './recentview';

class  TaskRoute extends React.Component{
  render(){
    return(
      <Switch>
      <Route path="/layout/task/statistic" component={TaskStatistic} />
      <Route exact path="/layout/task/taskmanage" component={TaskManage} />
      <Route exact path="/layout/task/aimmanage" component={AimManage} />
      <Route exact path="/layout/task/archivedlist" component={ArchivedList} />
      <Route exact path="/layout/task/taskdetail" component={TaskDetail} />
      <Route path="/layout/task/tasks/:taskId" component={Tasks} />
      <Route exact path="/layout/task/aimunfinish" component={AimUnfinish} />
      <Route exact path="/layout/task/aimfinish" component={AimFinish} />
      <Route exact path="/layout/task/taskunfinish" component={TaskUnfinish} />
      <Route exact path="/layout/task/taskfinish" component={TaskFinish} />
      <Route exact path="/layout/task/recentView" component={RecentView} />
      </Switch>
    )
  }
}

export default TaskRoute;
