import React from 'react';
import {BrowserRouter, Route , Switch, Redirect} from 'react-router-dom';
import Login from './route/User/login';
import Register from './route/User/register';
import LayoutComponent from './components/layout';
import AuthorizedRoute from './AuthorizedRoute';

class RouteFile extends React.Component{
  
  render(){
    return(
      <div>
       <BrowserRouter>
          <Switch>
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <Redirect from='/' exact to='/login' />
          <AuthorizedRoute path="/layout" component={LayoutComponent} />
          </Switch>
       </BrowserRouter>
       </div>
    );
  }
}

export default RouteFile;
