import React from 'react';
import {Table, Divider,Button} from 'antd';
import 'antd/dist/antd.css';
import axios from 'axios';
import {connect} from 'react-redux';

class Professpage extends React.Component {
  render(){
    return(
      <span>
      <Button onClick={()=>this.props.addData(this.props.data,this.props.name,this.props.link)}>{this.props.name}</Button>
      </span>
    );
  }
}
class TablesPage extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      username:null,
      data:[],
      columns:[]
    }
  }

  componentWillMount(){
    var userinfo = sessionStorage.getItem('userinfo');
      if (this.props.account===null) {
        this.setState({
          username: JSON.parse(userinfo).account
        });
      }else{
      this.setState({
        username: this.props.account
      });
    }
  }
  componentDidMount(){
    let self = this;
    axios({
      method: "GET",
      url: 'http://localhost:8901/app/contentfors?account='+this.state.username,
      data: []
    }).then(function(result) {
      let tasksname = result.data.map((item) =>
        item._source.task_name);
        axios({
        method:"GET",
        url:'http://localhost:8901/app/aims?aim_for='+tasksname,
        data:[]
          }).then(function(result) {
          let aimsdata=result.data.map((item)=>
            item._source);
        self.setState({
         data:aimsdata
             });
         }).catch(function(err){
           console.log(err);
         });
    }).catch(function(err) {
      console.log(err);
    });

      axios({
        method:"GET",
        url:'http://localhost:8901/app/fields?id=finish_aims',
        data:[]
      }).then(function(result) {
        self.setState({
         columns:result.data.columns
             });
         }).catch(function(err){
           console.log(err);
     });
  }


  render() {
    function Renderaction(addData){
      return (text,record)=>(
           <div>
           <Professpage addData={addData} name='文本分类' data={record} link="http://localhost:5601/app/kibana#/discover?_g=()&_a=(columns:!(_source),index:'4769e780-88d3-11e8-9c39-bb2d8a088bf0',interval:auto,query:(language:lucene,query:''),sort:!(_score,desc))"/>
           <Divider  key="divider1" type="vertical" />
           <Professpage addData={addData} name='信息溯源' data={record} link="http://localhost:5601/app/kibana#/visualize/edit/72fc0250-88d5-11e8-9c39-bb2d8a088bf0?_g=()&_a=(filters:!(),linked:!f,query:(language:lucene,query:''),uiState:(),vis:(aggs:!((enabled:!t,id:'1',params:(),schema:metric,type:count),(enabled:!t,id:'2',params:(field:user.keyword,missingBucket:!f,missingBucketLabel:Missing,order:desc,orderBy:'1',otherBucket:!f,otherBucketLabel:Other,size:5),schema:segment,type:terms)),params:(addLegend:!t,addTooltip:!t,isDonut:!t,labels:(last_level:!t,show:!f,truncate:100,values:!t),legendPosition:right,type:pie),title:tab2,type:pie))"/>
           <Divider  key="divider2" type="vertical" />
           <Professpage addData={addData} name='统计分析' data={record} link="http://localhost:5601/app/kibana#/visualize/edit/a316eb80-88d5-11e8-9c39-bb2d8a088bf0?embed=true&_g=()"/>
           <Divider  key="divider3" type="vertical" />
           <Professpage addData={addData} name='团伙发现' data={record} link="http://localhost:5601/app/kibana#/visualize/edit/dce532e0-88d5-11e8-9c39-bb2d8a088bf0?embed=true&_g=()"/>
           <Divider  key="divider4" type="vertical" />
           <Professpage addData={addData} name='地图' data={record} link="http://localhost:5601/app/kibana#/visualize/edit/63f157a0-88d6-11e8-9c39-bb2d8a088bf0?_g=()"/>
           </div>
       );
       }
  function Displaytime(columns){
       if(columns.key!=='reservation'){
           return (text,record)=>(
                <span>
                {new Date(text).toLocaleString()}
                </span>
            );
         }else{
           return (text,record)=>(
                <span>
                {new Date(text[0]).toLocaleDateString()+'~'+new Date(text[1]).toLocaleDateString()}
                </span>
            );
         }
       }
   function DispAimStatus(columns){
    let aim_state = {"0":"启动","1":"归档"};
    let aim_approve_state = {"0":"待审批","1":"通过","2":"拒绝"};
    let aim_option = {"0":"核查","1":"中标","2":"资源申请"};
    let aim_view = {"0":"只显号码","1":"只显内容","2":"全部显示"};

    switch (columns.key) {
    case 'aim_state':
     return (text,record)=>( <span>{aim_state[text]} </span>);
    //  break;
    case 'aim_approve_state':
     return (text,record)=>(<span>{aim_approve_state[text]} </span>);
    // break;
   case 'aim_option':
    return (text,record)=>( <span>{aim_option[text]} </span>);
    // break;
   case 'aim_view':
    return (text,record)=>(<span>{aim_view[text]} </span>);
    // break;
    default:
     return (text,record)=>(<span>{text}</span>);
         }
       }
    let columns=this.state.columns.slice();
    for(var i in columns){
      if(columns[i].type==="action"){
        columns[i]={title:columns[i].cn,key:columns[i].key,dataIndex:columns[i].key,display:columns[i].display,render:Renderaction(this.props.addData)};
      }else if(columns[i].type==="time"){
        columns[i]={title:columns[i].cn,key:columns[i].key,dataIndex:columns[i].key,display:columns[i].display,render:Displaytime(columns[i])};
      }else if(columns[i].type==="num"){
        columns[i]={title:columns[i].cn,key:columns[i].key,dataIndex:columns[i].key,display:columns[i].display,render:DispAimStatus(columns[i])};
      }else{
        columns[i]={title:columns[i].cn,key:columns[i].key,dataIndex:columns[i].key,display:columns[i].display};
      }
    }

    return (
      <div>
      <Table
      bordered
      columns={columns}
      dataSource={this.state.data.filter(item=>item.aim_for===this.props.pathname[4])}
      onChange={this.handleChange}
      />
      </div>
    );
}
}
export default connect(
  state=>state.UserInfo
)(TablesPage);
