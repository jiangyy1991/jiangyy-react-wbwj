const elasticsearch = require('elasticsearch');

module.exports = function(querystr,callback){
  const esClient_post = new elasticsearch.Client({
  host:'localhost:9200',
  log:'error'
});

    let aimsquery = {
          query: {
            bool: {
              must: [
                {
                  match_phrase:{
                    aim_for:querystr.taskname
                  }}
              ]
            }
          }
        }
    esClient_post.search({
      index:'aims',
      type:'wbwj',
      body:aimsquery,
    }).then(function(re){
      callback(re.hits.hits);
    },function(err){
      console.trace(err.message);
    });
}
