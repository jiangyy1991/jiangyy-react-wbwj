const elasticsearch = require('elasticsearch');

module.exports = function(querystr,callback){
  const esClient_post = new elasticsearch.Client({
  host:'localhost:9200',
  log:'error'
});
  esClient_post.search({
    index:"users",
    type:"wbwj",
    body:{
    query:{
      bool:{
        must:[
          {
            match:{
            account:querystr.username,
          }
        },{
            match:{
            password:querystr.password
          }
        }]
      }
    }
  }
  }).then(function(re){
    callback(re.hits.hits[0]);
  },function(err){
    console.trace(err.message);
  });

}
