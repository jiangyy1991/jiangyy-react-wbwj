export function AimsFinish(state = {finish_aims:null}, action){
  switch (action.type) {
    case 'FinishAimsData':
      return({
          finish_aims:action.finish_aims
        })
    default:
      return state
  }
}

export function AimFinish(finish_aims) {
  return {type:'FinishAimsData',finish_aims:finish_aims}
}
