import React from 'react';
import { Route, Redirect } from 'react-router-dom';

class AuthLog extends React.Component {
  render() {
    const { component: Component, ...rest } = this.props
    const isLog = JSON.parse(sessionStorage.getItem("userinfo")).roles.indexOf("3") === -1 ? true : false;
    return (
        <Route {...rest} render={props => {
          return isLog
              ? <Redirect to="/layout/403" />
              : <Component {...props} />
        }} />
    )
  }
}

export default AuthLog
