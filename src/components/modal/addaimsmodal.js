import React from 'react';
import FormList from './../form/formlist';
import {Form, Modal, Button} from 'antd';
import 'antd/dist/antd.css';
import axios from 'axios';
import Swal from 'sweetalert';

class  Addaimsmodal extends React.Component{
  constructor(props){
    super(props);
    this.state={
      visible:false,
    }
    this.showModal = this.showModal.bind(this);
    this.handleOk = this.handleOk.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  showModal(){
    this.setState({
      visible:true,
    });
  }

  handleOk(){
    const { getFieldsValue, resetFields } = this.props.form;
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
    var username = this.props.username;
    const values = getFieldsValue();
    var myDate = new Date();
    var uuid = "wbwj"+myDate.getFullYear()+myDate.getMonth()+myDate.getDate()+myDate.getHours()+myDate.getMinutes()+myDate.getSeconds()+myDate.getMilliseconds();
    //重置表单 (坑点)
    //此处发送ajax请求，向后台写数据
    values.key = uuid;
    values.aim_for = this.props.record.task_name;
    values.aim_create=username;
    values.aim_approve_state=0;
    values.aim_edittime=new Date().getTime();
    const refreshData = this.props.refreshData;
    axios({
      method:"POST",
      url:'http://localhost:8901/app/data',
      datatype:'json',
      data:{dataname:this.props.dataname,data:values}
        }).then(function(result) {
      Swal("新建成功！").then(function(){
          refreshData();
          axios({
            method:"POST",
            url:'http://localhost:8901/app/logstash',
            datatype:'json',
            data:{data:{user:username,action:'新增',time:new Date().getTime(),desp:'新增目标'+values.aim_name}}
              }).then(function(result) {
            console.log("插入日志");
          }).catch(function(err){
               console.log(err);
             });
          }
        )
       }).catch(function(err){
         console.log(err);
       });
    resetFields();
    this.setState({
      visible:false,
    });
  }
});
  }
  handleCancel(){
    const {resetFields}= this.props.form;
    resetFields();
    this.setState({
      visible:false,
    });
  }

  render(){
    let displays = this.props.display.slice();
    let disabled=[];
    for(var i in displays){
      if(displays[i].display!=='none'&&displays[i].key!=='aim_for'){
        disabled[displays[i].key]=false;
      }else {
        displays.splice(i, 1);
      }
    }

    return(
      <span>
      <Button type="primary" onClick={this.showModal}>{this.props.name}</Button>
      <Modal
      title={this.props.name}
      visible={this.state.visible}
      onOk={this.handleOk}
      onCancel={this.handleCancel}
      okText="确认"
      cancelText="取消"
      >
      <FormList disabled={disabled} form={this.props.form} display={displays}/>
      </Modal>
      </span>
    );
  }
}

export default Form.create()(Addaimsmodal);
