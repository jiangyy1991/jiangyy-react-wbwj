import React from 'react';
import {Layout} from 'antd';
const { Sider} = Layout;

class MainSider extends React.Component {
  // constructor(props) {
  //   super(props);
  // }

  render(){
    return(
      <Sider width={200} style={{ background: '#fff' ,border:'solid 1px rgb(0,0,0)'}}>
      {this.props.children}
      </Sider>
    )
  }
}

export default MainSider;
