const elasticsearch = require('elasticsearch');

module.exports = function(querystr,callback){
  const esClient_post = new elasticsearch.Client({
  host:'localhost:9200',
  log:'error'
});

    let aimsquery = {
          query: {
            bool: {
              must: [{
                terms:{
                  aim_for:querystr.aim_for.split(',')
                }
              },
              {
                match_phrase: {
                  aim_option: 0
                }
              },
                {
                  match_phrase: {
                    aim_approve_state: 1
                  }
                }
              ]
            }
          }
        }

    esClient_post.search({
      index:'aims',
      type:'wbwj',
      body:aimsquery,
      size:1000,
    }).then(function(re){
      callback(re.hits.hits);
    },function(err){
      console.trace(err.message);
    });
}
