const elasticsearch = require('elasticsearch');

module.exports = function(querystr,callback){
  const esClient_post = new elasticsearch.Client({
    host:'localhost:9200',
    log:'error'
});

  esClient_post.search({
    index:"tasks",
    type:"wbwj",
    body:{query:{match_all:{}}},
    _source:"task_name",
  }).then(function(re){
    callback(re.hits.hits);
  },function(err){
    console.trace(err.message);
  });

}
